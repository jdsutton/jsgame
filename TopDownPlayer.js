import('/src/JSUtils/math/Vector.js');
import('Player.js');

/**
 * @class
 */
class TopDownPlayer extends Player {

    /**
     * @public
     * @override
     */
    initControls() {
        ConfigurableController
            .setInput(
                Player.INPUT.JUMP,
                new KeyDownBooleanInput(KEYS.SPACE).or(
                    new GamepadButtonBooleanInput(0, 0)
                ),
            ).setInput(
                Player.INPUT.UP,
                new KeyDownBooleanInput(KEYS.W_KEY).or(
                    new GamepadAxisBooleanInput(0, 1, -1)
                ),
            ).setInput(
                Player.INPUT.DOWN,
                new KeyDownBooleanInput(KEYS.S_KEY).or(
                    new GamepadAxisBooleanInput(0, 1, 1)
                ),
            ).setInput(
                Player.INPUT.LEFT,
                new KeyDownBooleanInput(KEYS.A_KEY).or(
                    new GamepadAxisBooleanInput(0, 0, -1)
                ),
            ).setInput(
                Player.INPUT.RIGHT,
                new KeyDownBooleanInput(KEYS.D_KEY).or(
                    new GamepadAxisBooleanInput(0, 0, 1)
                ),
            )
        ;
            
        ConfigurableController
            .onInputPressed(Player.INPUT.UP, () => {
                this._onUp();
            })
            .onInputPressed(Player.INPUT.DOWN, () => {
                this._onDown();
            })
            .onInputPressed(Player.INPUT.LEFT, () => {
                this._onLeft();
            })
            .onInputPressed(Player.INPUT.RIGHT, () => {
                this._onRight();
            })
            .onInputReleased(Player.INPUT.UP, () => {
                this._onIdleUp();
            })
            .onInputReleased(Player.INPUT.DOWN, () => {
                this._onIdleDown();
            })
            .onInputReleased(Player.INPUT.LEFT, () => {
                this._onIdleLeft();
            })
            .onInputReleased(Player.INPUT.RIGHT, () => {
                this._onIdleRight();
            });

        // ConfigurableController.onInputPressed(Player.INPUT.JUMP, () => {
        //     if (!Game.canControlPlayer()) return;                
            
        //     if (this._canJump()) {
        //         this._jump();
        //     }
        // });

        // ConfigurableController.onInputReleased(Player.INPUT.JUMP, () => {
        //     if (!Game.canControlPlayer()) return

        //     if (this._jumpState === Player._JUMP_STATE.JUMP && this._vy < 0) {
        //         this._vy = 0;
        //     }
        // });

        return this;
    }

    /**
     * @private
     */
    _onUp() {}

    /**
     * @private
     */
    _onIdleUp() {}

    /**
     * @private
     */
    _onIdleDown() {}

    /**
     * @private
     */
    _onDown() {}

    /**
     * @public
     * @override
     */
    update() {
        if (!Game.canControlPlayer()) return;

        let x = 0;
        let y = 0;

        if (ConfigurableController.getInput(Player.INPUT.LEFT)) {
            x = -1;
        } else if (ConfigurableController.getInput(Player.INPUT.RIGHT)) {
            x = 1;
        }

        if (ConfigurableController.getInput(Player.INPUT.UP)) {
            y = -1;
        } else if (ConfigurableController.getInput(Player.INPUT.DOWN)) {
            y = 1;
        }

        const vector = new Vector(x, y);
        const speed = this._speed * vector.magnitude();
        const theta = vector.angle();

        this._vx = speed * Math.cos(theta);
        this._vy = speed * Math.sin(theta);

        super.update();
    }
}

TopDownPlayer._GRAVITY = 0;
