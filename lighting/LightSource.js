/**
 * @class
 */
class LightSource {

    /**
     * @constructor
     */
    constructor(x, y, width, height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * @public
     * @abstract
     */
    getMask() {}
}