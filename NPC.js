import('PhysObject.js');

/**
 * @class
 */
class NPC extends PhysObject {

    /**
     * @public
     */
    copy() {
        let result = new (this.constructor)(this.x, this.y, this.width, this.height);

        result._setAnimation(this._animation);

        return result;
    }

    /**
     * @public
     */
    toDict() {
        return {
            class: this.constructor.name,
            args: [this.x, this.y, this.width, this.height],
        }
    }

    /**
     * @public
     */
    static fromDict(d) {
        return new (eval(d.class))(...d.args);
    }

    /**
     * @public
     * @abstract
     */
    onPlayerCollision() {}
}