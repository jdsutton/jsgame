import('/src/JSUtils/math/Vector.js');
import('Prop.js');

/**
 * @class
 */
class PhysObject extends Prop {

    /**
     * @constructor
     */
    constructor(x, y, width, height) {
        super(x, y, width, height);

        this._vx = 0;
        this._vy = 0;
        this._physYMax = null;
        this.mass = 1;

        /** @type {Number} */
        this.gravity = this.constructor._GRAVITY;
    }

    /**
     * @private
     */
    _land() {
        this._vy = 0;
    }

    /**
     * @private
     * @abstract
     */
    _onCollideLeft() {}

    /**
     * @private
     * @abstract
     */
    _onCollideRight() {}

    onCollision(other, collisionGroup) {

    }

    /**
     * @private
     */
    _positionIsClear(xoff, yoff) {
        const room = this.game.currentRoom;
        const hitbox = this.hitbox;

        if (!room) return false;

        xoff = xoff || 0;
        yoff = yoff || 0;

        return room.rectIsClear(
            hitbox.x + xoff,
            hitbox.y + yoff,
            hitbox.width,
            hitbox.height,
            this,
            this._collideWithGroups,
        );
    }

    /**
     * @public
     */
    offsetVelocity(dvx, dvy) {
        this._vx += dvx;
        this._vy += dvy;

        return this;
    }

    /**
     * @private
     */
    _moveToClearPositionX() {
        if (this._positionIsClear()) return;

        let delta = 1;
        let newPosition;

        while (true) {

            if (this._positionIsClear(-delta, 0)) {
                newPosition = -delta;

                break;
            }

            if (this._positionIsClear(delta, 0)) {
                newPosition = delta;

                break;
            }

            delta++;
        }

        if (newPosition < 0) this._onCollideRight();
        else if (newPosition > 0) this._onCollideLeft();

        this._x += newPosition;
    }

    /**
     * @private
     */
    _moveToClearPositionY() {
        if (this._positionIsClear()) return;

        let delta = 1;
        let newPosition;

        while (true) {

            if (this._positionIsClear(0, -delta)) {
                newPosition = -delta;

                break;
            }

            if (this._positionIsClear(0, delta)) {
                newPosition = delta;

                break;
            }

            delta++;
        }

        if (newPosition < 0) this._land();

        this._y += newPosition;
    }

    /**
     * @public
     */
    magnetize(cls, distanceFactor, maxDistance, factor) {
        let totalForce = new Vector(0, 0);

        if (factor === undefined) factor = 1;

        this.game.currentRoom.props
            .filter(p => p !== this && p instanceof cls)
            .forEach(prop => {
                const toThis = new Vector(this.x - prop.x, this.y - prop.y);
                let distance = toThis.magnitude()

                if (distance < maxDistance) {
                    let direction;
                    distance *= distanceFactor;

                    try {
                        direction = toThis.normalize();
                    } catch (e) {
                        // Zero magnitude.
                        return;
                    }

                    const force = new Vector(direction.x, direction.y)
                        .divide(Math.pow(distance, 2) / (-1 * factor));

                    this._vx += force.x;
                    this._vy += force.y;
                }
            });
    }

    /**
     * @public
     */
    friction(factor) {
        this._vx *= factor;
        this._vy *= factor;
    }

    /**
     * @public
     */
    applyForce(vector) {
        const acc = vector.multiply(1 / this.mass);

        this._vx += acc.x;
        this._vy += acc.y;
    }
    
    /**
     * @public
     */
    update() {      
        if (this._positionIsClear(0, this.gravity)) {
            this._vy += this.gravity;
        }

        this._y += this._vy;
        this._moveToClearPositionY();

        this._x += this._vx;
        this._moveToClearPositionX();
    }
}

PhysObject._GRAVITY = 0.3;
