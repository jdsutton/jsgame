import('Drawable.js');
import('props/TileProp.js');
import('/src/JSUtils/math/Random.js');

/**
 * @class
 */
class AutoTilingMaterial extends Drawable {

    /**
     * @public
     */
    constructor(game, tilesetURI, tileWidth, tileHeight) {
        super();

        this.game = game;
        this._tilesetURI = tilesetURI;
        this._imageURI = this._tilesetURI;
        this._tileWidth = tileWidth || 16;
        this._tileHeight = tileHeight || 16;
        this._tileClass = TileProp;
        this._tilesByXY = {};

        this.name = 'New Auto-tiling Material';

        this._middleTiles = [];
        this._topRightCorners = [];
        this._bottomRightCorners = [];
        this._bottomLeftCorners = [];
        this._topLeftCorners = [];
        this._topSides = [];
        this._rightSides = [];
        this._bottomSides = [];
        this._leftSides = [];
        this._drawable = null;

        AutoTilingMaterial._materialsByName[this.constructor.name] = this;
    }

    /**
     * @public
     * @override
     */
    set width(val) {}

    /**
     * @public
     * @override
     */
    set height(val) {}

    /**
     * @public
     * @override
     */
    get width() {
        return this._tileWidth;
    }

    /**
     * @public
     * @override
     */
    get height() {
        return this._tileHeight;
    }

    /**
     * @public
     */
    static createFromImage(game, uri, x, y, tileSize) {
        return new AutoTilingMaterial(game, uri, tileSize)
            .addMiddleTile(x + tileSize, y + tileSize)
            .addTopRightCorner(x + tileSize*2, y + 0)
            .addTopLeftCorner(x + 0, y + 0)
            .addBottomRightCorner(x + tileSize*2, y + tileSize*2)
            .addBottomLeftCorner(x + 0, y + tileSize*2)
            .addTopSide(x + tileSize, y + 0)
            .addRightSide(x + tileSize*2, y + tileSize)
            .addBottomSide(x + tileSize, y + tileSize*2)
            .addLeftSide(x + 0, y + tileSize);
    }

    /**
     * @public
     */
    static get(name) {
        return this._materialsByName[name];
    }

    /**
     * @public
     */
    copy() {
        let result = new AutoTilingMaterial(this.game, this._tilesetURI, this.tileWidth, this.tileHeight);

        // FIXME: Must copy arrays.
        Object.assign(result, this);

        return result;
    }

    /**
     * @public
     */
    setName(s) {
        this.name = s;

        return this;
    }

    /**
     * @public
     * @param {Prop} c
     */
    setTileClass(c) {
        this._tileClass = c;

        return this;
    }

    /**
     * @private
     */
    _getTileForPosition(x, y) {
        let tiles = [];
        let neighbors = [];

        // Lookup neighboring positions, solid?
        for (let i = -1; i <= 1; i++) {
            let row = [];

            for (let j = -1; j <= 1; j++) {
                const xPos = x + this._tileWidth * i;
                const yPos = y + this._tileHeight * j;

                row.push(this.game.currentRoom.hasTileAtPosition(this.defaultLayer, xPos, yPos));
            }

            neighbors.push(row);
        }

        // Is inside block?
        let isInside = neighbors[1][0]
            && neighbors[1][2]
            && neighbors[0][1]
            && neighbors[2][1];

        if (isInside) {
            // Middle or inside corner.

            // TODO: Check inside corners
            
            // else.
            tiles = this._middleTiles;
        } else {
            // Match slope of edge.
            const xx = 1;
            const yy = 1;

            tiles = this._topSides;

            if (neighbors[xx][yy - 1] && neighbors[xx][yy + 1] && neighbors[xx - 1][yy]) {
                tiles = this._rightSides;
            } else if (neighbors[xx][yy - 1] && neighbors[xx][yy + 1] && neighbors[xx + 1][yy]) {
                tiles = this._leftSides;
            } else if (neighbors[xx - 1][yy] && neighbors[xx + 1][yy] && neighbors[xx][yy + 1]) {
                tiles = this._topSides;
            } else if (neighbors[xx - 1][yy] && neighbors[xx + 1][yy] && neighbors[xx][yy - 1]) {
                tiles = this._bottomSides;
            } else if (neighbors[xx - 1][yy] && neighbors[xx][yy + 1]) {
                tiles = this._topRightCorners;
            } else if (neighbors[xx][yy - 1] && neighbors[xx - 1][yy]) {
                tiles = this._bottomRightCorners;
            } else if (neighbors[xx][yy - 1] && neighbors[xx + 1][yy]) {
                tiles = this._bottomLeftCorners;
            }  else if (neighbors[xx][yy + 1] && neighbors[xx + 1][yy]) {
                tiles = this._topLeftCorners;
            }
        }

        const pos = Random.choice(tiles);

        return this._getTileFromTileset(pos[0], pos[1]);
    }

    /**
     * @public
     */
    handleDeleteAtPosition(x, y) {
        this._updateNeighbors(x, y, null, true);
    }


    /**
     * @public
     */
    placeTileAtPosition(x, y, visitedPositions, skipOnPlace) {
        if (!this._image) {
            throw new Error('Must call load().then() first');
        }
        
        if (x === undefined) x = this.x;
        if (y === undefined) y = this.y;

        const firstPlacement = !visitedPositions;
        visitedPositions = visitedPositions || [];

        for (let pos of visitedPositions) {
            // Stop if visited already.
            if (pos[0] === x && pos[1] === y) return;
        }

        visitedPositions.push([x, y]);

        const room = this.game.currentRoom;
        const otherTile = room.hasTileAtPosition(this.defaultLayer, x, y);

        if (otherTile) {
            room.removeDrawable(otherTile);
        }

        let tile = this._getTileForPosition(x, y);

        tile.defaultLayer = this.defaultLayer;
        tile.x = x;
        tile.y = y;
        tile.materialClassName = this.constructor.name;

        room.addTile(tile);

        if (visitedPositions.length === 1)
            this._updateNeighbors(x, y, visitedPositions, skipOnPlace);

        if (firstPlacement && !skipOnPlace) {
            tile.onPlace();
        }
    }

    /**
     * Update neighbors as required.
     * @private
     */
    _updateNeighbors(x, y, visitedPositions, skipOnPlace) {
        visitedPositions = visitedPositions || [];

        const sides = [[-1, 0], [1, 0], [0, -1], [0, 1]];
        const room = this.game.currentRoom;

        for (let pos of sides) {
            const neighborX = x + pos[0] * this._tileWidth;
            const neighborY = y + pos[1] * this._tileHeight;
            const otherTile = room.hasTileAtPosition(this.defaultLayer, neighborX, neighborY);

            if (otherTile && otherTile.materialClassName === this.constructor.name) {
                this.placeTileAtPosition(neighborX, neighborY, visitedPositions, skipOnPlace)
            }
        }
    }


    /**
     * @public
     */
    onPlace() {}

    /**
     * @public
     */
    draw() {
        if (this._drawable) {
            this._drawable.x = this.x;
            this._drawable.y = this.y;
            this._drawable.draw();
        } else {
            this.game.graphics.noFill();
            this.game.graphics.stroke(0, 255, 0);

            this.game.graphics.rect(this.x, this.y, this._tileWidth, this._tileHeight);
        }
    }

    /**
     * @private
     */
    _getTileFromTileset(x, y) {
        if (!this._tilesByXY[x]) {
            this._tilesByXY[x] = {};
        }

        if (!this._tilesByXY[x][y]) {
            let result = new (this._tileClass)(x, y);

            result.image = JSImage.crop(
                this._tilesetURI, x, y, this._tileWidth, this._tileHeight);

            this._tilesByXY[x][y] = result;
        }

        return this._tilesByXY[x][y].copy();
    }

    /** 
     * @public
     */
    addMiddleTile(x, y) {
        this._middleTiles.push([x, y]);

        JSImage.load(this._tilesetURI).then(() => {
            this._drawable = this._getTileFromTileset(x, y);
            this._drawable.game = this.game;
        });

        return this;
    }

    /**
     * @public
     */
    addTopRightCorner(x, y) {
        this._topRightCorners.push([x, y]);

        return this;
    }

    /**
     * @public
     */
    addTopLeftCorner(x, y) {
        this._topLeftCorners.push([x, y]);

        return this;
    }

    /**
     * @public
     */
    addBottomRightCorner(x, y) {
        this._bottomRightCorners.push([x, y]);

        return this;
    }

    /**
     * @public
     */
    addBottomLeftCorner(x, y) {
        this._bottomLeftCorners.push([x, y]);

        return this;
    }

    /**
     * @public
     */
    addTopSide(x, y) {
        this._topSides.push([x, y]);

        return this;
    }

    /**
     * @public
     */
    addRightSide(x, y) {
        this._rightSides.push([x, y]);

        return this;
    }

    /**
     * @public
     */
    addBottomSide(x, y) {
        this._bottomSides.push([x, y]);

        return this;
    }

    /**
     * @public
     */
    addLeftSide(x, y) {
        this._leftSides.push([x, y]);

        return this;
    }
}

AutoTilingMaterial._materialsByName = {};
