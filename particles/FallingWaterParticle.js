import('Particle.js');
import('/src/JSUtils/math/Random.js');

/**
 * @class
 */
class FallingWaterParticle extends Particle {

    /**
     * @constructor
     */
    constructor() {
        super(...arguments);

        this._gravity = 0.05;
        this._color = Random.choice(this.constructor._COLORS);

        this.defaultLayer = 10;
    }

    /**
     * @public
     */
    draw() {
        const ratio = this._getAliveTime() / this.constructor._LIFETIME_MS;
        const size = this.constructor._SIZE + this.constructor._EXPAND * ratio;
        
        let c = color(this._color);
        c.setAlpha(255 * (1 - ratio));

        noStroke();
        fill(c);

        rect(this.x, this.y, size, size * (1 + this._vy));
    }
}

FallingWaterParticle._LIFETIME_MS = 1000;
FallingWaterParticle._SIZE = 2;
FallingWaterParticle._EXPAND = 4;
FallingWaterParticle._COLORS = [
    '#1E88E5',
    '#B2EBF2',
    '#1A237E',
    '#00ACC1',
];
