import('Particle.js');

/**
 * @class
 */
class DamageCountParticle extends Particle {

    /**
     * @constructor
     */
    constructor(x, y, damage) {
        super(x, y, 1, -Math.PI / 2);

        this.defaultLayer = 3;
        this._damage = Number(damage).toFixed(2);

        while (this._damage.endsWith('0') || this._damage.endsWith('.'))
            this._damage = this._damage.slice(0, this._damage.length - 1);

        this._gravity = 0;
        this._friction = 0.01;
    }

    /** 
     * @private
     * @override
     */
    rotate(dTheta) {
        this._theta += dTheta;
    }

    /**
     * @public
     * @override
     */
    draw() {
        const a = 255 * (1 - this._getAliveTime() / this.constructor._LIFETIME_MS);
        const graphics = this.game.graphics;

        graphics.noStroke();
        graphics.fill(255, 255, 255, a);
        graphics.text(this._damage, this.x, this.y);
    }
}

DamageCountParticle._LIFETIME_MS = 1000;
