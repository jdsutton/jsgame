/**
 * @class
 */
class Particle extends Drawable {

    /**
     * @constructor
     */
    constructor(x, y, v, theta) {
        super(x, y, 1, 1, theta);

        this._v = v || 0;
        this._vx = Math.cos(theta || 0) * this._v;
        this._vy = Math.sin(theta || 0) * this._v;
        this._vxOff = 0;
        this._vyOff = 0;
        this._startTimeMs = new Date().getTime();
        this._killed = false;
        this._scale = 1;
        this._gravity = 0;
        this._friction = 0;
    }

    /**
     * @public
     */
    copy() {
        let result = new this.constructor(this._x, this._y, this._v, this._theta);

        result._vxOff = this._vxOff;
        result._vyOff = this._vyOff;
        result._startTimeMs = this._startTimeMs;
        result._scale = this._scale;
        result._gravity = this._gravity;
        result.damage = this.damage;

        return result;
    }

    /**
     * @public
     */
    get hitbox() {
        return {
            x: this._x,
            y: this._y,
            width: 1,
            height: 1,
        };
    }

    /**
     * @public
     */
    setVelocity(v, theta) {
        this._vx = v * Math.cos(theta);
        this._vy = v * Math.sin(theta);

        return this;
    }

    /**
     * @public
     */
    get velocity() {
        return new Vector(this._vx, this._vy).magnitude();
    }

    /**
     * @public
     */
    offsetVelocity(x, y) {
        this._vxOff += x;
        this._vyOff += y;

        return this;
    }

    /**
     * @public
     */
    setGravity(g) {
        this._gravity = g;
    }

    /**
     * @public
     */
    accelerate(v) {
        this._vxOff += Math.cos(this._theta) * v;
        this._vyOff += Math.sin(this._theta) * v;

        return this;
    }

    /**
     * @public
     */
    kill() {
        this._killed = true;

        return this;
    }

    /**
     * @private
     */
    _getAliveTime() {
        return new Date().getTime() - this._startTimeMs;
    }

    /**
     * @public
     */
    isAlive() {
        if (this._killed) return false;

        return this._getAliveTime() < this.constructor._LIFETIME_MS;
    }

    /**
     * @public
     */
    get theta() {
        return this._theta;
    }

    /**
     * @public
     */
    rotate(dTheta) {
        this._theta += dTheta;

        return this;
    }

    /**
     * @public
     */
    scale(ratio) {
        this._scale *= ratio;

        return this;
    }

    /**
     * @public
     */
    getScale() {
        return this._scale;
    }

    /**
     * @public
     */
    translate(x, y) {
        this._x += x;
        this._y += y;

        return this;
    }

    /**
     * @public
     */
    update() {
        this._vy += this._gravity;
        this._vx *= (1 - this._friction);
        this._vy *= (1 - this._friction);
        
        this._x += this._vxOff + this._vx;
        this._y += this._vyOff + this._vy;

        // this._theta = Math.atan2(this._vy + this._vyOff, this._vx + this._vyOff);
    }

    /**
     * @public
     */
    // draw() {
    //     super.draw();
        
    //     const graphics = this.game.graphics;

    //     graphics.stroke(255, 0, 0);
    //     graphics.noFill();
    //     graphics.ellipse(this._x, this._y, 6 * this._scale, 6 * this._scale);
    //     graphics.rect(this._x, this._y, 1, 1);
    // }

    /**
     * @public
     */
    static burst(x, y, v, theta, varianceV, varianceTheta, particleClass, num) {
        for (let i = 0; i < num; i++) {
            const t = theta + Random.range(-varianceTheta / 2, varianceTheta / 2);
            const pv = v + Random.range(-varianceV / 2, varianceV / 2);
            const p = new particleClass(x, y, pv, t);

            Game.currentRoom.addParticle(p);
        }
    }
}

Particle._LIFETIME_MS = 1000;