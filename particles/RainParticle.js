import('Particle.js');
import('/src/JSUtils/math/Random.js');

/**
 * @class
 */
class RainParticle extends Particle {

    /**
     * @constructor
     */
    constructor() {
        super(...arguments);

        this._gravity = 0.05;

        this.defaultLayer = 10;
    }

    /**
     * @public
     */
    draw() {
        stroke(3, 169, 244, 100);

        const v = new Vector(this._vx, this._vy)
            .multiply(3);

        line(this.x, this.y, this.x + v.x, this.y + v.y);
    }

    /**
     * @public
     */
    static rain(amount) {
        const room = Game.currentRoom;
        const margin = Game.viewWidth;

        for (let i = 0; i < amount; i++) {
            const x = Random.range(
                Game.viewX - margin,
                Game.viewX + Game.viewWidth + margin,
            );
            const p = new (this)(x, Game.viewY - margin, 1, Math.PI * 0.75);

            room.addParticle(p);
        }
    }
}

RainParticle._LIFETIME_MS = 5000;
