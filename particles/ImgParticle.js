import('Particle.js');
import('/src/JSUtils/math/Random.js');

/**
 * @class
 */
class ImgParticle extends Particle {

    /**
     * @constructor
     */
    constructor(x, y, image, imageX, imageY, w, h) {
        super(x, y, 1, Math.random() * Math.PI * 2);

        this._image = JSImage.crop(image, imageX, imageY, w, h);

        this._gravity = 0.12;
        this.defaultLayer = 0;
    }

    /** 
     * @private
     * @override
     */
    rotate(dTheta) {
        this._theta += dTheta;
    }

    /**
     * @public
     */
    static fromImage(x, y, image, size, particleClass) {
        let result = [];
        size = size || 2;
        particleClass = particleClass || ImgParticle;

        for (let i = x; i < x + image.width; i += size) {
            for (let j = y; j < y + image.height; j += size) {
                const p = new (particleClass)(i, j, image, i - x, j - y, size, size);

                result.push(p);
            }
        }

        return result;
    }

    /**
     * @public
     */
    static dissolve(x, y, image, theta, particleClass) {
        if (!this.game) {
            throw new Error('Set ImgParticle.game first');
        }

        let p = ImgParticle.fromImage(x, y, image, null, particleClass);

        p.forEach(part => {
            if (theta) {
                const t = theta + Random.range(-0.2, 0.2);
                const dv = Random.range(-1, 1);
                
                part.setVelocity(part._v + dv, t);
            }
            this.game.currentRoom.addParticle(part);
        });
    }
}

ImgParticle.game = null;
ImgParticle._LIFETIME_MS = 1000;
