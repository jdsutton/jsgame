import('Particle.js');
import('/src/JSUtils/math/Random.js');

/**
 * @class
 */
class SparkParticle extends Particle {

    /**
     * @constructor
     */
    constructor(x, y, v, theta) {
        super(x, y, v, theta);

        this._r = 255 - Math.random() * 100;
        this._g = 238 + Random.range(-30, 17);
        this._b = 88 + Random.range(-25, 25);
        this._gravity = 0.2;
    }

    /** 
     * @private
     * @override
     */
    rotate(dTheta) {
        this._theta += dTheta;
    }

    /**
     * @public
     * @override
     */
    draw() {
        stroke(this._r, this._g, this._b);
        point(this.x, this.y);
    }
}

SparkParticle._LIFETIME_MS = 1000;
