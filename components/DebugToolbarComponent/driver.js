import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class DebugToolbarComponent extends Component {

    /**
     * @constructor
     */
    constructor() {
        const params = {};
        
        super(DebugToolbarComponent.ID.TEMPLATE.THIS, params);
    }
}

DebugToolbarComponent.ID = {
    TEMPLATE: {
        THIS: 'DebugToolbarComponent_template',
    },
};
