import('/src/JSUtils/components/Component.js');
import('/src/jsgame/Game.js');

/**
 * @class
 */
class RoomPropertiesEditorComponent extends Component {

    /**
     * @constructor
     */
    constructor(game) {

        const params = {
            roomName: game.currentRoom.name,
        };
        
        super(RoomPropertiesEditorComponent.ID.TEMPLATE.THIS, params);
        this._game = game;

    }

    /**
     * @public
     */
    onRoomNameChanged() {
        const room = this._game.currentRoom;
        const index = this._game.world.indexOfRoom(room);
        const data = Dom.getDataFromForm(this.getId());

        this._game.world.setRoomName(index, data.roomName);
    }

    /**
     * @public
     */
    createNewRoom() {
        const room = new Room();

        this._game.setRoom(room).then(() => {
            this._params.roomName = room.name;

            this._game.graphics.background(0);

            this.update();
        });
    }
}

RoomPropertiesEditorComponent.ID = {
    TEMPLATE: {
        THIS: 'RoomPropertiesEditorComponent_template',
    },
};
