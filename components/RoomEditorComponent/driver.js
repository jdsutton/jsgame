import('/src/JSUtils/components/Component.js');
import('/src/JSUtils/components/TabManager.js');
import('/src/JSUtils/parse/Path.js');
import('/src/JSUtils/util/File.js');
import('/src/jsgame/editor/Editor.js');
import('/src/jsgame/JSImage.js');
import('/src/jsgame/props/TileProp.js');
import('/src/jsgame/worlds/WorldManager.js');

/**
 * @class
 */
class RoomEditorComponent extends Component {

    /**
     * @constructor
     */
    constructor(game, imageURIs, props, enemies) {
        const params = {};

        Editor.setGame(game);
        
        super(RoomEditorComponent.ID.TEMPLATE.THIS, params);
    
        this._game = game;
        this._tilesetImg = null;
        this._tilesets = [];
        this._tilesetConfigs = [];
        this._tilesetIndex = null;
        this._tileType = Tile.TYPE.NORMAL;
        this._tileClass = TileProp;
        this._tile = [];
        this._materials = [];
        this._tabManger = new TabManager()
            .addTab(this.getId() + this._ids().TAB.TILES, 'flex', 'flex')
            .addTab(this.getId() + this._ids().TAB.ENEMIES, 'none', 'flex')
            .addTab(this.getId() + this._ids().TAB.PROPS, 'none', 'flex')
            .addTab(this.getId() + this._ids().TAB.NPCS, 'none', 'flex')
            .addTab(this.getId() + this._ids().TAB.BG, 'none', 'flex')
            .addTab(this.getId() + this._ids().TAB.MUSIC, 'none', 'flex')
            .addTab(this.getId() + this._ids().TAB.ROOM, 'none', 'flex')
            .addTab(this.getId() + this._ids().TAB.MATERIALS, 'none', 'flex')
            .addTab(this.getId() + this._ids().TAB.LOAD_SAVE, 'none', 'flex');

        imageURIs = imageURIs || [];

        this._materialsComponent = new MaterialEditorComponent();

        this.setOnRender(() => {
            const enemiesTab = this.getElement(this._ids().TAB.ENEMIES);
            const propsTab = this.getElement(this._ids().TAB.PROPS);
            const npcsTab = this.getElement(this._ids().TAB.NPCS);
            const bgTab = this.getElement(this._ids().TAB.BG);
            const musicTab = this.getElement(this._ids().TAB.MUSIC);
            const roomTab = this.getElement(this._ids().TAB.ROOM);
            const materialsTab = this.getElement(this._ids().TAB.MATERIALS);

            Dom.setContents(enemiesTab, new EnemyEditorComponent(enemies).render());
            Dom.setContents(propsTab, new PropEditorComponent(props).render());
            Dom.setContents(npcsTab, new NPCEditorComponent().render());
            Dom.setContents(bgTab, new BGEditorComponent().render());
            Dom.setContents(musicTab, new MusicEditorComponent().render());
            Dom.setContents(roomTab, new RoomPropertiesEditorComponent(game).render());
            Dom.setContents(materialsTab, this._materialsComponent.render());

            imageURIs.forEach(uri => {
                this._addTilesetImage(uri);
            });
        });
    }

    /**
     * @public
     * @param {AutoTilingMaterial} material
     */
    addMaterial(material) {
        material.setTileClass(this._tileClass);

        this._materials.push(material);
        this._materialsComponent.addMaterial(material);

        return this;
    }

    /**
     * @public
     */
    setTileClass(c) {
        this._tileClass = c;

        this._materials.forEach(m => {
            m.setTileClass(c);
        });

        return this;
    }

    /**
     * @public
     */
    saveWorld() {
        return WorldManager.saveToLocalSystem();
    }

    /**
     * @public
     */
    saveRoom() {
        const room = this._game.currentRoom;
        const json = room.toJSON();

        File.save(json, `${room.name}.json`);
    }

    /**
     * @public
     */
    saveImage() {
        const room = this._game.currentRoom;
        const image = this._game.canvas
            .toDataURL('image/png').replace('image/png', 'image/octet-stream');

        File.save(image, `${room.name}.png`);
    }

    /**
     * @public
     */
    onSaveWorldToDropboxButtonClicked() {
        return WorldManager.saveToDropbox();
    }

    /**
     * @public
     */
    loadWorld() {
        return WorldManager.loadFromLocalSystem().then(() => {
            Game.transitionToRoom(Game.world.getRoom(0));
        });
    }

    /**
     * @public
     */
    onLoadWorldFromDropboxButtonClicked() {
        return WorldManager.loadFromDropbox().then(() => {
            Game.transitionToRoom(Game.world.getRoom(0));
        });
    }

    /**
     * @public
     */
    showTab(tabId) {
        this._tabManger.showTab(tabId);
    }

    /**
     * @public
     */
    selectTileType(tileType) {
        this._tileType = tileType;

        if (this._tile.length) {
            this._tile.forEach(tile => tile.setType(this._tileType));

            Editor.setPlaceableTile(this._tile);
        }
    }

    /**
     * @public
     */
    onLoadTilesetButtonClicked() {
        return File.select().then(uri => {
            this._addTilesetImage(uri);
        });
    }

    /**
     * @private
     */
    _addTilesetImage(uri) {
        let container = this.getElement(this._ids().TILESET_LIST);
        
        this._tilesetConfigs.push({
            offsetX: 0,
            offsetY: 0,
            tileWidth: 16,
            tileHeight: 16,
            tileColumnSpacing: 0,
            tileRowSpacing: 0,
        });

        Dom.appendTo(container, `<button>${uri}</button>`);
    }

    /**
     * @public
     */
    onTilesetListClicked(event) {
        const element = event.target;
        const parent = element.parentElement;
        const children = Array.from(parent.children);
       
        if (element.tagName.toLowerCase() !== 'button') return;

        // Compute tileset index.
        for (let i = 0; i < children.length; i++) {
            if (children[i] === element) {
                this._tilesetIndex = i;

                break;
            }
        }

        this._setTilesetImage(element);
    }

    /**
     * @private
     */
    _getConfig() {
        return this._tilesetConfigs[this._tilesetIndex];
    }

    /**
     * @private
     */
    _setTilesetImage(element) {
        // TODO: Fill in form values.
        let img = document.createElement('img');
        img.crossOrigin = 'anonymous';
        img.src = element.innerHTML;

        this._tilesetImg = img;

        JSImage.load(img.src).then(() => {
            this._drawTileset(true);
        });
    }

    /**
     * @public
     */
    _drawTileset(doGrid) {
        const element = this._tilesetImg;
        const config = this._getConfig();
        let canvas = this.getElement(this._ids().CANVAS);
        let context = canvas.getContext('2d');

        canvas.width = element.naturalWidth;
        canvas.height = element.naturalHeight;
        canvas.style.minWidth = element.naturalWidth + 'px';
        canvas.style.minHeight = element.naturalHeight + 'px';
        context.drawImage(element, 0, 0);

        if (doGrid) {
            context.strokeStyle = 'rgba(255,0,0,0.5)';

            const line = (x1, y1, x2, y2) => {
                context.beginPath();
                context.moveTo(x1, y1);
                context.lineTo(x2, y2);
                context.stroke();
            }

            // Draw grid.
            for (let x = config.offsetX; x <= canvas.width; x += config.tileWidth + config.tileColumnSpacing) {
                line(x, 0, x, canvas.height);
            }
            for (let y = config.offsetY; y <= canvas.height; y += config.tileHeight + config.tileRowSpacing) {
                line(0, y, canvas.width, y);
            }
        }
    }

    /**
     * @public
     */
    onCanvasClicked(event) {
        if (!this._tilesetImg) return;

        const config = this._getConfig();
        let canvas = this.getElement(this._ids().CANVAS);
        let context = canvas.getContext('2d');

        // Find tile.
        let x = Math.floor((event.offsetX + config.offsetX) / (config.tileWidth + config.tileColumnSpacing));
        let y = Math.floor((event.offsetY + config.offsetY) / (config.tileWidth + config.tileRowSpacing));
        x = x * (config.tileWidth + config.tileColumnSpacing) + config.offsetX;
        y = y * (config.tileHeight + config.tileRowSpacing) + config.offsetY;

        const tile = this._getTileFromCanvas(x, y, config.tileWidth, config.tileHeight);

        if (this._tile.length === 1)
            this._drawTileset(true);

        // Mark selection.
        context.strokeStyle = 'rgba(255,0,0,1)';
        context.rect(x, y, config.tileWidth, config.tileHeight);
        context.stroke();
        
        tile.load().then(() => {
            Editor.setPlaceableTile(this._tile);
        });
    }

    /**
     * @private
     */
    _getTileFromCanvas(x, y, width, height) {
        const tile = new (this._tileClass)(0, 0, width, height);

        tile.image = JSImage.crop(this._tilesetImg, x, y, width, height);

        if (Key.isDown(KEYS.CTRL)) {
            this._tile.push(tile);
        } else {
            this._tile = [tile];
        }

        return tile;
    }
}

RoomEditorComponent.ID = {
    CANVAS: '_canvas',
    TAB: {
        BG: '_bgTab',
        ENEMIES: '_enemiesTab',
        PROPS: '_propsTab',
        MUSIC: '_musicTab',
        NPCS: '_npcsTab',
        LOAD_SAVE: '_loadSaveTab',
        TILES: '_tilesTab',
        ROOM: '_roomTab',
        MATERIALS: '_materialsTab',
    },
    TILESET_LIST: '_tilesetList',
    TEMPLATE: {
        THIS: 'RoomEditorComponent_template',
    },
};
