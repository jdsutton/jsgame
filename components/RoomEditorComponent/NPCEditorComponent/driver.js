import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class NPCEditorComponent extends Component {

    /**
     * @constructor
     */
    constructor() {
        const params = {};
        
        super(NPCEditorComponent.ID.TEMPLATE.THIS, params);

        this._npc = null;

        this.setOnRender(() => {
            this._renderNPCButtons();
        });
    }

    /**
     * @private
     */
    _renderNPC(prop) {
        let result = document.createElement('button');

        result.onclick = () => {
            this._npc = new (eval(prop.class))(0, 0);

            this._npc.load().then(() => {
                Editor.setPlaceableProp(this._npc);
            });
        };

        Dom.setContents(result, prop.title);

        return result;
    }

    /**
     * @private
     */
    _renderNPCButtons() {
        const container = this.getElement(this._ids().NPC_LIST);

        this.constructor.NPCS.forEach(prop => {
            const button = this._renderNPC(prop);

            container.appendChild(button)
        });
    }
}

NPCEditorComponent.NPCS = [
    {
        title: 'Dr. Kline',
        class: 'Researcher',
    },
];

NPCEditorComponent.ID = {
    NPC_LIST: '_npcList',
    TEMPLATE: {
        THIS: 'NPCEditorComponent_template',
    },
};
