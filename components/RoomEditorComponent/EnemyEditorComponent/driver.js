import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class EnemyEditorComponent extends Component {

    /**
     * @constructor
     */
    constructor(enemies) {
        const params = {};
        
        super(EnemyEditorComponent.ID.TEMPLATE.THIS, params);

        this._enemies = enemies;

        this.setOnRender(() => {
            this._renderEnemyButtons();
        });
    }

    /**
     * @private
     */
    _renderEnemy(enemy) {
        let result = document.createElement('button');

        result.onclick = () => {
            const e = new (eval(enemy.class))(0, 0);

            e.load().then(() => {
                Editor.setPlaceableEnemy(e);
            });
        };

        Dom.setContents(result, enemy.title);

        return result;
    }

    /**
     * @private
     */
    _renderEnemyButtons() {
        const container = this.getElement(this._ids().ENEMY_LIST);

        this._enemies.forEach(enemy => {
            const button = this._renderEnemy(enemy);

            container.appendChild(button)
        });
    }
}

EnemyEditorComponent.ID = {
    ENEMY_LIST: '_enemyList',
    TEMPLATE: {
        THIS: 'EnemyEditorComponent_template',
    },
};
