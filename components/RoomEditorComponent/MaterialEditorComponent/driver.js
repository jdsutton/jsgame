import('/src/JSUtils/components/Component.js');
import('/src/JSUtils/util/Dom.js');

/**
 * @class
 */
class MaterialEditorComponent extends Component {

    /**
     * @constructor
     */
    constructor(materials) {
        super(MaterialEditorComponent.ID.TEMPLATE.THIS, {});

        this._buttons = [];

        this.setOnRender(() => {
            const container = Dom.getById(this.getId());

            this._buttons.forEach(button => {
                container.appendChild(button);
            });
        });
    }

    /** 
     * @public
     */
    addMaterial(material) {
        let button = document.createElement('button');

        Dom.setContents(button, material.name);

        button.onclick = () => {
            this._selectMaterial(material);
        };

        this._buttons.push(button);
    }

    /**
     * @private
     */
    _selectMaterial(material) {
        Editor.setPlaceableMaterial(material);
    }
}

MaterialEditorComponent.ID = {
    TEMPLATE: {
        THIS: 'MaterialEditorComponent_template',
    },
};
