import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class MusicEditorComponent extends Component {

    /**
     * @constructor
     */
    constructor(musicURIs) {
        const params = {};
        
        super(MusicEditorComponent.ID.TEMPLATE.THIS, params);

        this._uris = musicURIs || [];

        this.setOnRender(() => {
            this._renderSongs();
        });
    }

    /**
     * @private
     */
    _renderSongs() {
        const keys = Array.from(Object.keys(this._uris));
        let container = Dom.getById(this.getId());

        keys.forEach(key => {
            const uri = URI.MUSIC[key];
            let button = document.createElement('button');
            button.innerHTML = key;

            button.addEventListener('click', () => {
                if (!Planetoid.currentRoom) return;

                Planetoid.currentRoom.setMusic(uri);
            });

            container.appendChild(button);
        });
    }
}

MusicEditorComponent.ID = {
    TEMPLATE: {
        THIS: 'MusicEditorComponent_template',
    },
};
