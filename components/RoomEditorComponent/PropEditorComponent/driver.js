import('/src/JSUtils/components/Component.js');
import('/src/jsgame/ConfigurableController.js');
import('/src/jsgame/Player.js');

/**
 * @class
 */
class PropEditorComponent extends Component {

    /**
     * @constructor
     */
    constructor(props) {
        const params = {};
        
        super(PropEditorComponent.ID.TEMPLATE.THIS, params);

        this._prop = null;
        this._props = props;

        ConfigurableController.onInputReleased(Player.INPUT.AIM_DOWN, () => {
            if (!this._prop) return;

            this._prop.rotate(-Math.PI / 2);
        });

        ConfigurableController.onInputReleased(Player.INPUT.AIM_UP, () => {
            if (!this._prop) return;

            this._prop.rotate(Math.PI / 2);
        });

        this.setOnRender(() => {
            this._renderPropButtons();
        });
    }

    /**
     * @private
     */
    _renderProp(prop) {
        let result = document.createElement('button');

        result.onclick = () => {
            this._prop = new (eval(prop.class))(0, 0);

            this._prop.load().then(() => {
                Editor.setPlaceableProp(this._prop);
            });
        };

        Dom.setContents(result, prop.title);

        return result;
    }

    /**
     * @private
     */
    _renderPropButtons() {
        const container = this.getElement(this._ids().PROP_LIST);

        this._props.forEach(prop => {
            const button = this._renderProp(prop);

            container.appendChild(button)
        });
    }
}

PropEditorComponent.ID = {
    PROP_LIST: '_propList',
    TEMPLATE: {
        THIS: 'PropEditorComponent_template',
    },
};
