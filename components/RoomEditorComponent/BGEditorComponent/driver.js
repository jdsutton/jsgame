import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class BGEditorComponent extends Component {

    /**
     * @constructor
     */
    constructor(backgroundURIs) {
        const params = {};
        
        super(BGEditorComponent.ID.TEMPLATE.THIS, params);

        this._uris = backgroundURIs || [];

        this.setOnRender(() => {
            this._renderImages();
        });
    }

    /**
     * @private
     */
    _renderImages() {
        const uris = Array.from(Object.values(this._uris));
        let container = Dom.getById(this.getId());

        uris.forEach(uri => {
            let img = document.createElement('img');
            img.src = uri[0];

            img.addEventListener('click', () => {
                if (!Planetoid.currentRoom) return;

                Planetoid.currentRoom.setBackgroundLayers(uri);
            });

            container.appendChild(img);
        });
    }
}

BGEditorComponent.ID = {
    TEMPLATE: {
        THIS: 'BGEditorComponent_template',
    },
};
