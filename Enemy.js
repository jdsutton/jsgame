import('PhysObject.js');
import('particles/DamageCountParticle.js');
import('particles/ImgParticle.js');
import('/src/JSUtils/math/Vector.js');

/**
 * @class
 */
class Enemy extends PhysObject {

    /**
     * @constructor
     */
    constructor() {
        super(...arguments);

        this.health = 1;
        this.defense = 0;
        this.damage = 1;
        this._vx = this.constructor._WALK_SPEED;
        // this._animation = new Animation();
        this._offset = 0;
        this._hurtTime = null;
        this._args = [...arguments];
        this._loadPromise = null;
        this._flashOnHurt = true;
        this._onHurtCallbacks = [];
    }

    /**
     * @public
     */
    setup() {
        this._vx = this.constructor._WALK_SPEED;
    }

    /**
     * @public
     */
    get hurtbox() {
        return this.hitbox;
    }

    /**
     * @public
     */
    dissolve(theta) {
        let frame;

        if (this._animation) frame = this._animation.getFrame();
        frame = frame || this._img || this._image;

        if (!frame) return;

        ImgParticle.dissolve(this.x, this.y, frame, theta);
    }

    /**
     * @public
     */
    isAlive() {
        return this.health > 0;
    }

    /**
     * @public
     */
    isSolid() {
        return true;
    }

    /**
     * @private
     */
    _onHurt(p, damage) {
        const part = new DamageCountParticle(p.x, p.y, damage);

        this.game.currentRoom.addParticle(part);
    }

    /**
     * @public
     */
    onHurt(callback) {
        this._onHurtCallbacks.push(callback);

        return this;
    }

    /**
     * @private
     */
    _onKill(p) {
        if (!p) return;
        
        const x = this.x + this.width / 2;
        const y = this.y + this.height / 2;
        const theta = p.theta || Math.atan2(y - p.y, x - p.x);

        this.dissolve(theta);
    }

    /**
     * @private
     * @abstract
     */
    _onFailedHurt() {}

    /**
     * @public
     */
    onProjectileCollision(p) {
        if (!p.isAlive()) return;
        if (!p.hurtEnemies) return;
        if (this.health <= 0) return;
        
        const damage = Math.max(0, p.damage - this.defense);

        this._onHurt(p, damage);
        this._onHurtCallbacks.forEach(c => c());

        if (damage > 0) {
            this.health -= damage;
            this._hurtTime = new Date().getTime();

            if (this.health <= 0) this._onKill(p);
        } else {
            this._onFailedHurt();
        }

        p.kill();
    }

    /**
     * @public
     */
    onPlayerCollision(player) {
        if (!player.collidesWithRect(this.hurtbox)) return;

        player.hurt(this, this.damage);
    }

    /**
     * @public
     */ 
    copy() {
        let result = new this.constructor(this._x, this._y, this.width, this.height);

        if (this._animation)
            result._animation = this._animation.copy();
        result._vx = this._vx;
        result._image = this._image;
        result.defaultLayer = this.defaultLayer;

        return result;
    }

    /**
     * @private
     * @override
     */
    _onCollideLeft() {
        this._vx = this.constructor._WALK_SPEED;
        this._flipHorizontal = false;
    }

    /**
     * @private
     * @override
     */
    _onCollideRight() {
        this._vx = -1 * this.constructor._WALK_SPEED;
        this._flipHorizontal = true;
    }

    /**
     * @private
     * @override
     */
    _setAnimation(animation) {
        return super._setAnimation(animation).then(frame => {
            this.hitbox.width = frame.width;
            this.hitbox.height = frame.height;
        });
    }

    // /**
    //  * @public
    //  */
    // draw(filter, filterParam) {
    //     let frame = this._animation.getFrame(this._offset);

    //     if (filter) {
    //         frame = JSImage.filter(frame, filter, filterParam);
    //     }

    //     if (frame) {
    //         if (this._flashOnHurt) {
    //             if (new Date().getTime() - this._hurtTime < this.constructor._HURT_DURATION_MS) {
    //                 frame = JSImage.filter(frame, INVERT);
    //             }
    //         }

    //         const x = Math.round(this._x + this.width / 2 - frame.width / 2);
    //         const y = Math.round(this._y + this.height - frame.height);

    //         this._drawImg(frame, x, y);
    //     }

    //     if (Game.debugMode) {
    //         const graphics = this.game.graphics;

    //         graphics.stroke(255);
    //         graphics.noFill();
    //         graphics.rect(this.hitbox.x, this.hitbox.y, this.hitbox.width, this.hitbox.height);

    //         graphics.stroke(255, 0, 0);
    //         graphics.noFill();
    //         graphics.rect(this.hurtbox.x, this.hurtbox.y, this.hurtbox.width, this.hurtbox.height);
    //     }
    // }
}

Enemy._ANIMATION = {};
Enemy._WALK_SPEED = 1;
Enemy._HURT_DURATION_MS = 100;
Enemy._PERSIST_PROPERTIES.push('_vx');
Enemy._PERSIST_PROPERTIES.push('_vy');
