/**
 * @class
 */
class StateTimer {

    /**
     * @constructor
     */
    constructor(durationsMsByState, onChange) {
        this._states = Array.from(Object.keys(durationsMsByState));
        this._durations = durationsMsByState;
        this._stateIndex = -1;
        this._state = null;
        this._interval = null;

        this._updateState();
        
        this._onChange = onChange;
    }

    /**
     * @private
     */
    _updateState(randomize) {
        this._stateIndex = (this._stateIndex + 1) % this._states.length;
        this._state = this._states[this._stateIndex];

        let duration = this._durations[this._state];

        if (randomize) {
            duration *= Math.random();
        }

        this._interval = setTimeout(() => {
            this._updateState();
        }, duration);

        if (this._onChange) this._onChange();
    }

    /**
     * @public
     */
    randomize() {
        this._stateIndex = Math.floor(Math.random() * this._states.length);

        if (this._interval) clearInterval(this._interval);

        this._updateState(true);

        return this;
    }

    /**
     * @public
     */
    getState() {
        return this._state;
    }
}