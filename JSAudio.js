import('animation/Animate.js');

/**
 * @class
 */
class JSAudio {

    /**
     * @public
     */
    static load(uri, callback) {
        if (!this._PROMISES_BY_URI[uri]) {
            this._PROMISES_BY_URI[uri] = new Promise((resolve, reject) => {
                let audio = new Audio(uri);

                this._AUDIO_BY_URI[uri] = audio;

                audio.addEventListener('canplaythrough', () => {

                    if (callback) callback(uri, audio);

                    resolve(audio);
                });

                audio.addEventListener('stalled', e => {
                    reject(audio);
                });

                audio.addEventListener('suspended', e => {
                    reject(audio);
                });

                audio.addEventListener('error', e => {
                    reject(audio);
                });

                return audio;
            });
        }

        return this._PROMISES_BY_URI[uri];
    }

    /**
     * @public
     */
    static loadAll(dict, callback) {
        const keys = Object.keys(dict);
        let promises = [];

        keys.forEach(key => {
            promises.push(this.load(dict[key], callback));
        });

        return Promise.all(promises);
    }

    /**
     * @public
     */
    static fadeIn(uri, copy, volume, loop) {
        let audio = this._getAudio(uri);

        if (!audio) {
            console.error('Could not play ' + uri);

            return;
        }

        this.playFx(uri, copy, 0, loop);

        Animate.variable(audio, 'volume', 0, volume, 1000);
    }

    /**
     * @public
     * @return {Promise}
     */
    static playFx(uri, copy, volume, loop) {
        let audio = this._getAudio(uri);

        if (!audio) {
            console.error('Could not play ' + uri);

            return;
        }

        if (volume !== undefined) {
            audio.volume = volume;
        }

        if (loop) {
            audio.loop = true;
        }

        if (copy) {
            let a = new Audio();
            
            a.src = audio.src;

            audio = a;
        }

        try {
            return audio.play();
        } catch (e) {
            console.error(e);

            return Promise.resolve();
        }
    }

    /**
     * @public
     */
    static stopFx(audio) {
        if (!audio) return;

        let result = this._getAudio(audio);

        result.pause();
        result.currentTime = 0;
    }

    /**
     * @public
     * Trigger all audio, useful for certain browsers.
     * Call this method when the user interacts with the page for the first time.
     */
    static unlockAll() {
        this.listFx().forEach(uri => {
            this.playFx(uri, null, 0).then(() => {
                this.stopFx(uri);
            });
        });
    }

    /**
     * @public
     */
    static listFx() {
        return Array.from(Object.keys(this._AUDIO_BY_URI));
    }

    /**
     * @private
     */
    static _getAudio(audio) {
        if (!audio) {
            console.warn('Invalid audio reference: ' + audio);
            
            return null;
        }

        if (typeof audio === 'string' || audio instanceof String) {

            const result = this._AUDIO_BY_URI[audio];

            if (!result) {
                console.warn('No audio loaded with uri: ' + audio);
            }

            return result;
        }

        return audio;
    }
}

JSAudio._AUDIO_BY_URI = {};
JSAudio._PROMISES_BY_URI = {};