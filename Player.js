import('ConfigurableController.js');
import('PhysObject.js');
import('/src/JSUtils/io/Keys.js');

/**
 * @class
 */
class Player extends PhysObject {

    /**
     * @constructor
     */
    constructor(x, y) {
        super(x, y);

        this._xDir = 1;
        this._state = Player._STATE.STAND;
        this._jumpState = Player._JUMP_STATE.LAND;
        this._aimState = Player._AIM_STATE.FORWARD;
        this._invulnerable = false;
        this.health = this.constructor._DEFAULT_HEALTH;
        this._jumpDisabled = false;
        this._speed = this.constructor._RUN_SPEED;
    }

    /**
     * @public
     */
    setRunSpeed(speed) {
        this._speed = speed;

        return this;
    }

    /**
     * @public
     */
    setJumpEnabled(value) {
        this._jumpDisabled = !value;

        return this;
    }

    /**
     * @public
     */
    init() {
        this.health = this.constructor._DEFAULT_HEALTH;
    }

    /**
     * @public
     */
    recharge() {
        this.health = this.constructor._DEFAULT_HEALTH;
    }

    /**
     * @public
     */
    loadStateFromDict(dict) {
        Object.assign(this, dict);
    }

    /**
     * @public
     */
    initControls() {
        ConfigurableController
            .setInput(
                Player.INPUT.JUMP,
                new KeyDownBooleanInput(KEYS.SPACE).or(
                    new GamepadButtonBooleanInput(0, 0)
                ),
            ).setInput(
                Player.INPUT.UP,
                new KeyDownBooleanInput(KEYS.W_KEY).or(
                    new GamepadAxisBooleanInput(0, 1, -1)
                ),
            ).setInput(
                Player.INPUT.DOWN,
                new KeyDownBooleanInput(KEYS.S_KEY).or(
                    new GamepadAxisBooleanInput(0, 1, 1)
                ),
            ).setInput(
                Player.INPUT.LEFT,
                new KeyDownBooleanInput(KEYS.A_KEY).or(
                    new GamepadAxisBooleanInput(0, 0, -1)
                ),
            ).setInput(
                Player.INPUT.RIGHT,
                new KeyDownBooleanInput(KEYS.D_KEY).or(
                    new GamepadAxisBooleanInput(0, 0, 1)
                ),
            ).setInput(
                Player.INPUT.AIM_UP,
                new KeyDownBooleanInput(KEYS.Z_KEY).or(
                    new GamepadButtonBooleanInput(0, 7)
                ),
            ).setInput(
                Player.INPUT.AIM_DOWN,
                new KeyDownBooleanInput(KEYS.X_KEY).or(
                    new GamepadButtonBooleanInput(0, 6)
                ),
            ).setInput(
                Player.INPUT.MOD_1,
                new KeyDownBooleanInput(KEYS.Q_KEY).or(
                    new GamepadButtonBooleanInput(0, 3)
                )
            ).setInput(
                Player.INPUT.MOD_2,
                new KeyDownBooleanInput(KEYS.F_KEY).or(
                    new GamepadButtonBooleanInput(0, 2)
                )
            ).setInput(
                Player.INPUT.MOD_3,
                new KeyDownBooleanInput(KEYS.Y_KEY).or(
                    new GamepadButtonBooleanInput(0, 12)
                )
            ).setInput(
                Player.INPUT.MOD_4,
                new KeyDownBooleanInput(KEYS.U_KEY).or(
                    new GamepadButtonBooleanInput(0, 13)
                )
            ).setInput(
                Player.INPUT.RB,
                new KeyDownBooleanInput(KEYS.R_KEY).or(
                    new GamepadButtonBooleanInput(0, 5)
                )
            ).setInput(
                Player.INPUT.SELECT,
                new KeyDownBooleanInput(KEYS.SHIFT).or(
                    new GamepadButtonBooleanInput(0, 8)
                )
            )
        ;
            
        ConfigurableController
            .onInputPressed(Player.INPUT.UP, () => {
                if (!this.game.canControlPlayer()) return;

                this._onUp();
            })
            .onInputReleased(Player.INPUT.UP, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputPressed(Player.INPUT.AIM_UP, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputReleased(Player.INPUT.AIM_UP, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputPressed(Player.INPUT.AIM_DOWN, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputReleased(Player.INPUT.AIM_DOWN, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputReleased(Player.INPUT.LEFT, () => {
                this.idle();
            })
            .onInputReleased(Player.INPUT.RIGHT, () => {
                this.idle();
            });

        ConfigurableController.onInputPressed(Player.INPUT.JUMP, () => {
            if (!this.game.canControlPlayer()) return;                
            
            if (this._canJump()) {
                this._jump();
            }
        });

        ConfigurableController.onInputReleased(Player.INPUT.JUMP, () => {
            // TODO: Refactor into this.game.
            if (this.game.cutsceneIsActive()) {
                this.game.advanceCutscene();
            
                return;                
            }
            if (!this.game.canControlPlayer()) return

            if (this._jumpState === Player._JUMP_STATE.JUMP && this._vy < 0) {
                this._vy = 0;
            }
        });

        ConfigurableController.onInputPressed(Player.INPUT.DOWN, () => {
            if (!this.game.canControlPlayer()) return;

            this._crouch();
        })
        .onInputReleased(Player.INPUT.DOWN, () => {
            if (!this.game.canControlPlayer()) return;
            
            this._updateAimState();
        });
    }

    /**
     * @private
     * @abstract
     */
    _onHurt() {}


    /**
     * @private
     * @abstract
     */
    _onLoseInvulnerability() {}

    /**
     * @public
     */
    hurt(enemy, damage) {
        if (this._invulnerable || damage <= 0) return;

        const dir = Math.sign(
            (this.x + this.width / 2) - (enemy.x + enemy.width / 2))
            * this.constructor._IMPACT_VELOCITY;

        const v = new Vector(dir, -this.constructor._JUMP_VELOCITY);

        this.health -= damage;

        this._vx = v.x;
        this._vy = v.y;

        this._invulnerable = true;

        this._onHurt();

        setTimeout(() => {
            this._invulnerable = false;

            this._onLoseInvulnerability();
        }, this.constructor._INVULNERABLE_TIME_MS);
    }

    /**
     * @public
     */
    onProjectileCollision(p) {
        if (!p.hurtPlayer) return;
        
        p.kill();

        this.hurt(p, p.damage);
    }

    /**
     * @public
     */
    isSolid() {
        return true;
    }

    /**
     * @private
     */
    _aim(newState) {
        const prevAimState = this._aimState;

        this._aimState = newState;

        if (this._aimState !== prevAimState) {
            this._onAimStateChanged();
            // this.idle();
        }
    }

    /**
     * @public
     */
    _updateAimState() {
        if (this.game.mouseIsInsideGameCanvas()) return;

        const up = ConfigurableController.getInput(Player.INPUT.UP);
        const down = ConfigurableController.getInput(Player.INPUT.DOWN);

        if (up) {
            this._aim(Player._AIM_STATE.UP);
        } else if (ConfigurableController.getInput(Player.INPUT.AIM_UP)) {
            this._aim(Player._AIM_STATE.UP_OVER);
        } else if (ConfigurableController.getInput(Player.INPUT.AIM_DOWN)) {
            this._aim(Player._AIM_STATE.DOWN_OVER);
        } else {
            this._aim(Player._AIM_STATE.FORWARD);
        }
    }

    /**
     * @public
     */
    asDict() {
        return {
            x: this.x,
            y: this.y,
        };
    }

    /**
     * @private
     */
    _onLeft() {}

    /**
     * @private
     * @abstract
     */
    _onIdleLeft() {}

    /**
     * @private
     */
    _onRight() {}

    /**
     * @private
     * @abstract
     */
    _onIdleRight() {}

    /**
     * @private
     * @abstract
     */
    _onCrouch() {}

    /**
     * @private
     * @abstract
     */
    _onCrawl() {}

    /**
     * @private
     * @abstract
     */
    _onAimStateChanged() {}

    /**
     * @private
     */
    getLauncherOffset() {
        return {
            x: 0,
            y: 0,
        };
    }

    /**
     * @private
     */
    _getMoveSpeed() {
        return this._speed;
    }

    /**
     * @public
     */
    getAimTheta() {
        let theta = 0;

        if (this._aimState === Player._AIM_STATE.UP_OVER) {
            theta = -Math.PI / 4;
        } else if (this._aimState === Player._AIM_STATE.DOWN_OVER) {
            theta = Math.PI / 4;
        } else if (this._aimState === Player._AIM_STATE.UP) {
            theta = -Math.PI / 2;
        } else if (this._aimState === Player._AIM_STATE.DOWN) {
            theta = Math.PI / 2;
        }

        if (this._xDir < 0) theta = Math.PI - theta;

        return theta;
    }

    /**
     * @private
     */
    _crouch() {
        if (this.game.paused) return;

        if (this._state === Player._STATE.STAND) {
            this._state = Player._STATE.CROUCH;
            this._onCrouch();
        } else if (this._state === Player._STATE.CROUCH) {
            this._state = Player._STATE.CRAWL;
            this._onCrawl();
        } else {
            this._updateAimState();
        }
    }

    /**
     * @public
     */
    idle() {
        if (this.game.paused) return;

        this._vx = 0;

        if (this._xDir < 0) {
            this._onIdleLeft();
        } else {
            this._onIdleRight();
        }
    }

    /**
     * @private
     */
    _canStandUp() {
        return true;
    }

    /** 
     * @private
     */
    _onUp() {
        if (!this.game.canControlPlayer()) return;
        if (!this._canStandUp()) return;

        if (this._state === Player._STATE.CRAWL) {
            this._state = Player._STATE.CROUCH;

            this.idle();
        } else if (this._state === Player._STATE.CROUCH) {
            this._state = Player._STATE.STAND;

            this.idle();
        } else {
            this._updateAimState();
        }
    }

    /**
     * @public
     * @override
     */
    update() {
        if (!this.game.canControlPlayer()) return;

        super.update();

        if (!this._positionIsClear(0, this.constructor._GRAVITY)
            && this._jumpState === Player._STATE.JUMP) {
            this._land();
        }

        if (this._vy !== 0) this._jumpState = Player._JUMP_STATE.JUMP;
    
        this._handlePropCollisions();
        this._handleEnemyCollisions();
    }

    /**
     * @private
     */
    _handlePropCollisions() {
        const props = this.game.currentRoom.props;

        for (let prop of props) {
            if (prop.collidesWithRect(this.hitbox)) {
                prop.onPlayerCollision(this);
            }
        }
    }

    /**
     * @private
     */
    _handleEnemyCollisions() {
        const enemies = this.game.currentRoom.enemies;

        for (let enemy of enemies) {
            if (enemy.collidesWithRect(this.hitbox)) {
                enemy.onPlayerCollision(this);
            }
        }
    }


    /**
     * @private
     */
    _canJump() {
        return !this._jumpDisabled
            && this._jumpState !== Player._JUMP_STATE.JUMP
            && this._positionIsClear(0, -Player._JUMP_VELOCITY);
    }

    /**
     * @private
     */
    _getJumpVelocity() {
        return this.constructor._JUMP_VELOCITY;
    }

    /**
     * @private
     * @abstract
     */
    _onJump() {};

    /**
     * @private
     */
    _jump() {
        if (this.game.paused) return;

        if (this._state === Player._STATE.CROUCH) {
            this._state = Player._STATE.STAND;
        } else {
            this._jumpState = Player._JUMP_STATE.JUMP;
            this._vy = -this._getJumpVelocity();

            this._onJump();
        }
    }

    /**
     * @private
     * @override
     */
    _land() {
        this._vy = 0;
        this._jumpState = Player._JUMP_STATE.LAND;
    }

    /**
     * @public
     */
    drawHUD() {
        const hitbox = this.hitbox;
        const total = this.health / this.constructor._MAX_HEALTH;
        const barWidth = 64;
        const barHeight = 8;
        const pad = 8;

        fill(0, 0, 255);
        noStroke();

        rect(pad, pad, 100 * total, barHeight);

        strokeWeight(1);
        noFill();
        stroke(0);
        line(pad, pad, pad + barWidth, pad);
        line(pad, pad, pad, pad + barHeight);
        stroke(255);
        line(barWidth + pad, pad, barWidth + pad, pad + barHeight);
        line(pad, pad + barHeight, barWidth + pad, pad + barHeight);
    }
}

Player._GRAVITY = 0.15;
Player._JUMP_VELOCITY = 4;
Player._RUN_SPEED = 2.5;
Player._SHOT_VELOCITY = 6;
Player._DEFAULT_HEALTH = 100;
Player._MAX_HEALTH = 100;
Player._IMPACT_VELOCITY = 1;
Player._INVULNERABLE_TIME_MS = 500;
Player._FIRE_DELAY_MS = 300;

Player._PERSIST_PROPERTIES = [
    '_x',
    '_y',
    '_width',
    '_height',
    'health',
];

Player.INPUT = {
    JUMP: 'jump',
    DOWN: 'down',
    UP: 'up',
    LEFT: 'left',
    RIGHT: 'right',
    FIRE: 'fire',
    AIM_UP: 'aim_up',
    AIM_DOWN: 'aim_down',
    MOD_1: 'mod_1',
    MOD_2: 'mod_2',
    MOD_3: 'mod_3',
    MOD_4: 'mod_4',
    SELECT: 'select',
    RB: 'right_button',
};

Player._STATE = {
    STAND: 'stand',
    CROUCH: 'crouch',
    CRAWL: 'crawl',
    JUMP: 'jump',
};

Player._JUMP_STATE = {
    JUMP: 'jump',
    LAND: 'land',
};

Player._AIM_STATE = {
    UP: 'up',
    UP_OVER: 'up_over',
    FORWARD: 'forward',
    DOWN_OVER: 'down_over',
    DOWN: 'down',
};