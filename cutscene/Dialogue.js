import('CutsceneEvent.js');

/**
 * @class
 */
class Dialogue extends CutsceneEvent {

    /**
     * @constructor
     */
    constructor(x, y, w, h, lines, textDelay, config) {
        super();

        config = config || {};
        this._x = x;
        this._y = y;
        this._w = w;
        this._h = h;
        this._lines = lines;
        this._i = 0;
        this._tOff = null;
        this._textDelay = textDelay || Dialogue._TEXT_TYPE_DELAY_MS;
        this._textSize = config.textSize || 12;
        this._textFont = config.textFont || 'Helvetica';
        this._textColor = config.textColor || Dialogue._COLORS_BY_NAME['white'];
        this._activeTextColor = this._textColor;
        this._characterSpacing = config.characterSpacing || this._textSize;
        this._lineSpacing = config.lineSpacing || this._textSize;
        this._callbacksByLineAndCharacterIndex = [];
        this._pad = config.pad || 8;
        this._finished = false;

        this._parseLines();
    }

    /**
     * @public
     */
    setTextDelay(ms) {
        this._textDelay = ms;

        return this;
    }

    /**
     * @private
     */
    _parseLines() {
        for (let i = 0; i < this._lines.length; i++) {
            this._callbacksByLineAndCharacterIndex[i] = [];

            for (let j = 0; j < this._lines[i].length; j++) {
                const line = this._lines[i];

                if (line[j] === '{') {
                    const endIndex = line.indexOf('}', j);
                    const name = line.slice(j + 1, endIndex);
                    this._lines[i] = line.slice(0, j) + line.slice(endIndex + 1);

                    if (name in Dialogue._COLORS_BY_NAME) {
                        const callback = () => {
                            this._activeTextColor = Dialogue._COLORS_BY_NAME[name];
                        };

                        this._callbacksByLineAndCharacterIndex[i][j] = callback;
                    }
                }
            }
        }
    }

    /**
     * @public
     * @override
     */
    isFinished() {
        return this._finished;
    }

    /**
     * @public
     */
    isTyping() {
        const line = this._lines[this._i];

        return this._getJ() < line.length;
    }

    /**
     * @public
     * @override;
     */
    advance() {
        if (this._getJ() < this._lines[this._i].length - 1) {
            if (this._tOff !== 0) {
                // Skip to end.
                this._tOff = 0;

                return;
            }
        } else {
            this._tOff = new Date().getTime();
        }

        this._i++;

        if (this._i >= this._lines.length) {
            this._i = this._lines.length - 1;

            this._finished = true;
        }
    }

    /**
     * @private
     */
    _getT() {
        if (this._tOff === null) this._tOff = new Date().getTime();

        return new Date().getTime() - this._tOff;
    }

    /**
     * @private
     */
    _getJ() {
        return Math.floor(this._getT() / this._textDelay);
    }

    /**
     * @public
     */
    getLineY(i) {
        return this._y + this._pad + (this._lineSpacing * (i + 1));
    }

    /**
     * @public
     */
    get lineSpacing() {
        return this._lineSpacing;
    }

    /**
     * @public
     */
    draw() {
        const t = this._getT();
        const j = this._getJ();
        const pad = this._pad;
        let s = this._lines[this._i].slice(0, j);
        let y = this._y + this._pad + this._lineSpacing;
        let xOff = 0;
        let lastLineBreakIndex = 0;

        if (Math.floor(t / 500) % 2 === 0) {
            s = s + '|';
        } else {
            s = s + ' ';
        };

        noStroke();

        // Draw bg.
        // fill(0);
        // rect(this._x, this._y, this._w, this._h);

        const isWhitespace = char => {
            return ' \n\t'.indexOf(char) > -1;
        };

        textFont(this._textFont);
        textSize(this._textSize);
        this._activeTextColor = this._textColor;

        for (let i = 0; i < s.length; i++) {
            const callback = this._callbacksByLineAndCharacterIndex[this._i][i];
            const x = this._x + this._characterSpacing * i;
            
            const lineBreak = () => {
                xOff = this._characterSpacing * i;
                y += this._lineSpacing;
                lastLineBreakIndex = i;
            };

            if (s[i] === '\n') {
                lineBreak();

                continue;
            } else if (s[i - 1] === ' ') {
                // Pre-emptive line break.
                for (let j = i; j < s.length; j++) {
                    if (j < s.length - 1 && isWhitespace(s[j])) break;

                    if ((j - lastLineBreakIndex) * this._characterSpacing > (this._w - pad * 2)) {
                        lineBreak();
                    }
                }
            }

            if (callback) callback();
            fill(this._activeTextColor);

            text(
                s[i],
                x + pad - xOff,
                y,
            );
        }
    }
}

Dialogue._TEXT_TYPE_DELAY_MS = 50;
Dialogue._COLORS_BY_NAME = {
    'red': '#FF0000',
    'white': '#FFF',
};