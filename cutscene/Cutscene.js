
/**
 * @class
 */
class Cutscene {

    /**
     * @constructor
     */
    constructor(events) {
        this._events = events;
        this._i = 0;
        this._onFinishedCallback = null;
        this._finished = false;
    }

    /**
     * @public
     */
    reset() {
        this._i = 0;

        return this;
    }

    /**
     * @public
     */
    addEvent(event) {
        this._events.push(event);
    }

    /**
     * @public
     */
    isFinished() {
        return this._i >= this._events.length;
    }

    /**
     * @public
     */
    finish() {
        if (this._finished) return;

        this._finished = true;
        
        this._i = this._events.length;

        if (this._onFinishedCallback)
            this._onFinishedCallback();

        return this;
    }

    /**
     * @public
     */
    setOnFinished(callback) {
        this._onFinishedCallback = callback;

        return this;
    }

    /**
     * @public
     */
    advance() {
        const event = this._events[this._i];

        if (event) {
            event.advance();

            if (event.isFinished()) {
                this._i++;
            }
        }

        if (this.isFinished()) {
            this.finish();
        }
    }

    /**
     * @public
     */
    draw() {
        const event = this._events[this._i];

        if (!event) return;
        if (!event.isPlaying()) event.begin();

        event.draw();
    }
}

