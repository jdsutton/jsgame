/**
 * @class
 */
class CutsceneEvent {

    /**
     * @constructor
     */
    constructor() {
        this._active = false;
        this._startTime = null;
    }

    /**
     * @public
     */
    begin() {
        this._active = true;
        this._startTime = new Date().getTime();

        return this;
    }

    /**
     * @public
     */
    isPlaying() {
        return this._active;
    }

    /**
     * @public
     * @abstract
     */
    isFinished() {}

    /**
     * @public
     * @abstract
     */
    advance() {}

    /**
     * @public
     * @abstract
     */
    draw() {}
}
