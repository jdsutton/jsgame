import('/src/JSUtils/io/Mouse.js');
import('/src/JSUtils/io/Touch.js');
import('Drawable.js');

/**
 * @class
 * Clickable, draggable virtual input.
 */
class Handle extends Drawable {

    /**
     * @constructor
     */
    constructor(getBoundingBox, canvas) {
        super();

        this._id = Handle.getNextId();
        this._getBoundingBox = getBoundingBox;
        this._onRelease = [];
        this._onDragRight = [];
        this._onDragLeft = [];
        this._onDragUp = [];
        this._onDragDown = [];
        this._onDrag = [];
        this._onClickCallbacks = [];

        console.assert(canvas !== undefined);
        this._canvas = canvas;
        this.held = false;

        Handle._handles.push(this);
    }

    /**
     * @private
     */
    _onMove(x, y, dx, dy) {
        if (dx > 0) {
            this._onDragRight.forEach(f => f(dx));
        } else if (dx < 0) {
            this._onDragLeft.forEach(f => f(dx));
        }

        if (dy > 0) {
            this._onDragDown.forEach(f => f(dy));
        } else if (dy < 0) {
            this._onDragUp.forEach(f => f(dy));
        }

        this._onDrag.forEach(f => f(x, y, dx, dy));
    }

    /**
     * @public
     */
    tearDown() {
        const index = Handle._handles.indexOf(this);

        if (index > -1)
            Handle._handles.splice(index, 1);
    }

    /**
     * @public
     */
    cursorIsInside() {
        let pos;

        try {
            pos = Mouse.getPositionRelativeToElement(
                null,
                this._canvas,
            );
        } catch (e) {
            // console.warn(e);

            return false;
        }

        return this.pointIsInside(pos.x, pos.y);
    }

    /**
     * @public
     */
    touchIsInside() {
        let pos;

        try {
            pos = Touch.getPositionRelativeToElement(
                null,
                this._canvas,
            );
        } catch (e) {
            // console.warn(e);

            return false;
        }

        return this.pointIsInside(pos.x, pos.y);
    }

    /**
     * @public
     * @override
     */
    get hitbox() {
        return this._getBoundingBox();
    }

    /**
     * @public
     */
    inputIsInside() {
        return this.cursorIsInside() || this.touchIsInside();
    }

    /**
     * @public
     */
    isActive() {
        return this.held || this.inputIsInside();
    }

    /**
     * @public
     */
    draw() {
        const box = this._getBoundingBox();

        if (this.held) {
            fill('rgba(255, 255, 255, 0.5)');
            noStroke();
        } else {
            noFill();
            stroke(255);
        }

        rect(box.x, box.y, box.width, box.height);
    }

    /**
     * @public
     */
    _onClick(obj) {
        obj = obj || Mouse;

        const pos = obj.getPositionRelativeToElement(
            null,
            this._canvas,
        );

        this._onClickCallbacks.forEach(f => f(pos));
    }

    /**
     * @public
     */
    onClick(f) {
        this._onClickCallbacks.push(f);

        return this;
    }

    /**
     * @public
     */
    onRelease(f) {
        this._onRelease.push(f);

        return this;
    }

    /**
     * @public
     */
    onDragRight(f) {
        this._onDragRight.push(f);

        return this;
    }

    /**
     * @public
     */
    onDragLeft(f) {
        this._onDragLeft.push(f);

        return this;
    }

    /**
     * @public
     */
    onDragUp(f) {
        this._onDragUp.push(f);

        return this;
    }

    /**
     * @public
     */
    onDragDown(f) {
        this._onDragDown.push(f);

        return this;
    }

    /**
     * @public
     */
    onDrag(f) {
        this._onDrag.push(f);

        return this;
    }

    /**
     * @public
     */
    static getNextId() {
        const result = this._nextId;

        this._nextId++;

        return result;
    }

    /**
     * If a handle is currently held, return it.
     * Else, return the handle being hovered over.
     * Else return null.
     * @public
     * @return {?Handle}
     */
    static getActiveHandle() {
        if (this._heldHandle) return this._heldHandle;

        for (let handle of this._handles) {
            if (handle.inputIsInside()) return handle;
        }

        return null;
    }

    /**
     * @private
     */
    static _onPress(obj) {
        if (Handle._lock) return;

        obj = obj || Mouse;

        for (let handle of Handle._handles) {
            if (handle.inputIsInside()) {
                Handle._heldHandle = handle;
                Handle._lock = true;

                handle.held = true;
                handle._onClick(obj);

                break;
            }
        }
    }

    /**
     * @private
     */
    static _onDrag(event, dx, dy, obj) {
        if (!Handle._heldHandle) return;

        obj = obj || Mouse;

        const pos = obj.getPositionRelativeToElement(
            null,
            Handle._heldHandle._canvas,
        );

        Handle._heldHandle._onMove(pos.x, pos.y, dx, dy);
    }

    /**
     * @private
     */
    static _onRelease(obj) {
        Handle._lock = false;
        
        if (!Handle._heldHandle) return;

        Handle._heldHandle.held = false;

        obj = obj || Mouse;

        const pos = obj.getPositionRelativeToElement(
            null,
            Handle._heldHandle._canvas,
        );

        if (Handle._heldHandle.inputIsInside()) {
            Handle._heldHandle._onRelease.forEach(f => f(pos));
        }

        Handle._heldHandle = null;
    }

    /**
     * @public
     */
    static initTouch(touch) {
        this._touch = touch;

        this._touch.onStart(() => this._onPress(touch));
        this._touch.onMove((event, dx, dy) => this._onDrag(event, dx, dy, touch));
        this._touch.onEnd(() => this._onRelease(touch));
    }
}

Handle._lock = false;
Handle._nextId = 1;
Handle._handles = [];
Handle._heldHandle = null;
Handle._touch = Touch;

Mouse.onPress(() => Handle._onPress());
Mouse.onMove((event, dx, dy) => Handle._onDrag(event, dx, dy));
Mouse.onRelease(() => Handle._onRelease());

window.Handle = Handle;