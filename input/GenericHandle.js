import('../Handle.js');

/**
 * @class
 * Clickable, draggable virtual input.
 */
class GenericHandle extends Handle {

    /**
     * @constructor
     */
    constructor(pointIsInsideF, canvas) {
        super(null, canvas);

        this._pointIsInsideF = pointIsInsideF;
    }

    /**
     * @public
     * @override
     */
    pointIsInside(x, y) {
        return this._pointIsInsideF(x, y);
    }
}
