import('Animation.js');
import('Dictable.js');
import('JSAudio.js');
import('JSImage.js');
import('/src/JSUtils/math/Vector.js');

/**
 * @class
 */
class Drawable extends Dictable {

    /**
     * @constructor
     */
    constructor(x, y, width, height, theta, scale) {
        super();
        
        this._x = Math.round(x) || 0;
        this._y = Math.round(y) || 0;
        this._vx = 0;
        this._vy = 0;
        this.width = width || 16;
        this.height = height || 16;
        this._theta = 0;
        this._scale = scale || 1;

        if (theta)
            this.rotate(theta);
        
        this._imageURI = null;
        this._image = null;
        this._flipHorizontal = false;
        this.defaultLayer = 0;
        this._args = [...arguments];
        this._labels = {};
        this.game = null;
        this._collideWithGroups = [];
        this.collisionGroups = [];
        this.children = [];
        this.childLayers = [];
    }

    /**
     * @public
     * @param {Drawable} child
     * @param {Number} layer
     * @return {Drawable} this
     */
    addChild(child, layer) {
        this.children.push(child);
        this.childLayers.push(layer || 1);

        return this;
    }

    /**
     * @public
     */
    get image() {
        return this._image;
    }

    /**
     * @public
     */
    set image(v) {
        this._image = v;
    }

    /**
     * @public
     */
    setImage(v, setSize) {
        if (setSize === undefined) setSize = true;

        this._image = v;

        if (setSize) {
            this.width = v.naturalWidth * this._scale;
            this.height = v.naturalHeight * this._scale
        }

        return this;
    }

    /**
     * @public
     */
    get theta() {
        return this._theta;
    }

    /**
     * @public
     */
    set theta(val) {
        this._theta = val;
    }

    /**
     * @public
     */
    get hitbox() {
        return {
            x: this.x,
            y: this.y,
            width: this.width,
            height: this.height,
        }
    }

    /**
     * @public
     */
    set hitbox(val) {
        this._x = val.x;
        this._y = val.y;
        this.width = val.width;
        this.height = val.height;
    }

    /**
     * @public
     */
    get x() {
        return Math.round(this._x);
    }

    /**
     * @public
     */
    get y() {
        return Math.round(this._y);
    }

    /**
     * @public
     */
    set x(val) {
        this.children.forEach((child) => {
            const diff = child.x - this.x;

            child.x = val + diff;
        });

        this._x = val;
    }

    /**
     * @public
     */
    set y(val) {
        this.children.forEach((child) => {
            const diff = child.y - this.y;

            child.y = val + diff;
        });

        this._y = val;
    }

    /**
     * @public
     */
    get centerX() {
        return this.x + this.width / 2;
    }

    /**
     * @public
     */
    get centerY() {
        return this.y + this.height / 2;
    }

    /**
     * @public
     */
    get vx() {
        return this._vx;
    }

    /**
     * @public
     */
    set vx(val) {
        this._vx = val;
    }

    /**
     * @public
     */
    get vy() {
        return this._vy;
    }

    /**
     * @public
     */
    set vy(val) {
        this._vy = val;
    }

    /**
     * @public
     */
    scale(factor) {
        this._scale *= factor;

        return this;
    }

    /**
     * @public
     */
    setScale(scale) {
        this._scale = scale;

        return this;
    }
    /**
     * @public
     */
    draw() {
        const graphics = this.game.graphics;
        const x = this.x;
        const y = this.y;

        if (this.game.debugMode) {
            graphics.stroke('#FFF');
            graphics.noFill();
            graphics.ellipse(x, y, 10, 10);
            graphics.rect(this.hitbox.x, this.hitbox.y, this.hitbox.width, this.hitbox.height);
        }

        if (!(this._image || this._animation)) return;

        const frame = this._image || this._animation.getFrame();

        graphics.push();
        graphics.translate(x, y);
        if (this.theta) {
            const factor = this._scale / 2;
            
            graphics.translate(this.width * factor, this.height * factor);
            graphics.rotate(this.theta);
            graphics.translate(-this.width * factor, -this.height * factor);
        }

        if (this._flipHorizontal) {
            graphics.translate(this.width, 0);
            graphics.scale(-1, 1);
        }

        this._drawImg(frame);

        graphics.pop();
    }

    /**
     * @public
     */
    update() {
        this._x += this._vx;
        this._y += this._vy;
    }

    /**
     * @private
     */
    _normalizeRect(rect) {
        if (rect.width < 0) {
            rect.x += rect.width;
            rect.width *= -1;
        }

        if (rect.height < 0) {
            rect.y += rect.height;
            rect.height *= -1;
        }

        return rect;
    }

    /**
     * @public
     */
    pointIsInside(x, y, box) {
        box = box || this.hitbox;

        box = this._normalizeRect(box);

        return x >= box.x && x < box.x + box.width
            && y >= box.y && y < box.y + box.height;
    }

    /**
     * @public
     */
    collidesWithRect(rect) {
        const hitbox = this.hitbox;

        rect = this._normalizeRect(rect);

        return (rect.x < hitbox.x + hitbox.width
            && rect.x + rect.width > hitbox.x
            && rect.y < hitbox.y + hitbox.height
            && rect.y + rect.height > hitbox.y);
    }

    // /**
    //  * @private
    //  */
    // _drawAnimation(animation, x, y) {
    //     animation.render(
    //         Math.round(x),
    //         Math.round(y),
    //     );
    // }

    /**
     * @public
     */
    rotate(theta, newWidth, newHeight) {
        if (!newWidth || !newHeight) {
            newWidth = this.width;
            newHeight = this.height;
        }

        let v = new Vector(newWidth, newHeight)
            .rotateZ(theta);

        newWidth = Math.round(Math.abs(v.x));
        newHeight = Math.round(Math.abs(v.y));

        this.width = newWidth;
        this.height = newHeight;

        const full = Math.PI * 2;
        this._theta = (full + this._theta + theta) % (full);
    
        return this;
    }

    /**
     * @public
     */
    setAnimation(animation) {
        this._setAnimation(animation);

        return this;
    }

    /**
     * @private
     */
    _setAnimation(animation) {
        const shift = this._animation;
        this._animation = animation;
        this._image = null;
        
        return animation.load().then(() => {
            const frame = animation.getFrame();
            const theta = this._theta;
            
            this._theta = 0;
            this.rotate(theta, frame.width * this._scale, frame.height * this._scale);

            return frame;
        });
    }

    /**
     * @private
     */
    _trySetAnimation(animation) {
        const frame = animation.getFrame();
        const box = this.hitbox;
        box.width = frame.width;
        box.height = frame.height;

        if (Game.currentRoom.rectIsClear(box)) {
            this._setAnimation(animation);

            return true;
        }

        return false;
    }

    /**
     * @private
     */
    _drawImg(img) {
        const graphics = this.game.graphics;

        graphics.push();

        graphics.scale(this._scale);

        if (this._svg) {
            graphics.drawImage(img, 0, 0, this.width, this.height);
        } else {
            graphics.drawImage(img, 0, 0);        
        }

        graphics.pop();
    }

    /**
     * @public
     */
    static loadImage(uri) {
        return JSImage.load(uri);
    }

    /**
     * @public
     */
    load() {
        let promises = [
            JSImage.loadAllImages(Object.values(this.constructor._IMAGE_URI)),
            Animation.loadAll(this.constructor._ANIMATION),
            JSAudio.loadAll(this.constructor._SOUND_FX, (key, audio) => {
                this.constructor._SOUND_FX_BY_URI[key] = audio;
            }),
        ];

        if (this._imageURI) {
           this._svg = this._imageURI.endsWith('.svg');

            const p = Drawable.loadImage(this._imageURI).then(img => {
                this.setImage(img, !this._svg);
            });

            promises.push(p);

        }

        return Promise.all(promises);
    }

    /**
     * @public
     */
    isSolid() {
        return false;
    }

    /**
     * @public
     * @abstract
     */
    onProjectileCollision(projectile) {}

    /**
     * @public
     * @abstract
     */
    setup() {}

    /**
     * @public
     * @abstract
     */
    tearDown() {}

    /**
     * @public
     */
    getLabel(key) {
        return this._labels[key];
    }

    /**
     * @public
     */
    setLabel(key, value) {
        this._labels[key] = value;
    }
}

Drawable._IMAGE_URI = {};
Drawable._ANIMATION = {};
Drawable._SOUND_FX = {};
Drawable._SOUND_FX_BY_URI = {};
Drawable._PERSIST_PROPERTIES = [
    '_x',
    '_y',
    'width',
    'height',
    '_theta',
];
