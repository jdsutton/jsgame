import('Drawable.js');

/**
 * @class
 */
class Prop extends Drawable {

    /**
     * @constructor
     */
    constructor() {
        super(...arguments);

        this.defaultLayer = 0;

        this._animation = null;
        this._collideWithGroups = [
            Game.COLLISION.TILES,
        ];
    }

    /**
     * @public
     */
    copy() {
        let result = new this.constructor(this.x, this.y, this.width, this.height, this._theta,
            this._scale);

        result._animation = this._animation;
        result._image = this._image;
        result.defaultLayer = this.defaultLayer;

        return result;
    }

    /**
     * @public
     */
    isSolid() {
        return false;
    }

    /**
     * @public
     * @abstract
     */
    onPlayerCollision() {}

    /**
     * @public
     * @abstract
     */
    onPlace() {}
}

Prop._ANIMATION = {};
Prop._SOUND_FX = {};
Prop._AUDIO = {};
