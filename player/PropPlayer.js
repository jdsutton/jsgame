import('../Prop.js');

/**
 * @class
 */
class PropPlayer extends Prop {

    /**
     * constructor
     */
    constructor(player, prop) {
        super();

        this._player = player;
        this._prop = prop;
    }

    get x() {
        return this._player.x;
    }

    set x(val) {
        this._player.x = val;
    }

    get y() {
        return this._player.y;
    }

    set y(val) {
        this._player.y = val;
    }

    get vx() {
        return this._player._vx;
    }

    set vx(val) {
        this._player.vx = val;
    }

    get vy() {
        return this._player._vy;
    }

    set vy(val) {
        this._player.vy = val;
    }

    /**
     * @public
     * @override
     */
    draw() {
        this._prop.draw();
    }

    /**
     * @public
     * @override
     */
    drawHUD() {
        this._player.drawHUD();
    }

    /**
     * @public
     * @override
     */
    update() {
        this._prop.update();
        this._player.update();

        this._prop.x = this._player.x;
        this._prop.y = this._player.y;
    }
} 