import('CollisionGroup.js');
import('ConfigurableController.js');
import('editor/Editor.js');
import('Handle.js');
import('Player.js');
import('Room.js');
import('/src/JSUtils/graphics/Shapes.js');
import('/src/JSUtils/io/Keys.js');
import('/src/JSUtils/io/Mouse.js');

/**
 * @class
 */
class Game {

    /**
     * @public
     */
    static save() {

    }

    /**
     * @public
     */
    static listSaves() {

    }

    /**
     * @private
     */
    static _tryStart() {
        
    }

    /**
     * @public
     */
    static load() {
        // TODO: Stalling or not resolving on Safari.
        // Stalls if triggered before user interaction.
        // Otherwise just never resolves or rejects? 
        if (!this.currentRoom) this.setRoom(new Room());

        const audioPromise = JSAudio.loadAll(this._AUDIO);
        let promises = [];

        if (this.currentRoom) {
            promises.push(this.currentRoom.load());
        }

        if (this._IMG) {
            const uris = Object.values(this._IMG);

            promises.push(JSImage.loadAllImages(uris));
        }

        // Fuuuuck ios.
        const ios = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);

        if (!ios) {
            promises.push(audioPromise);
        }

        return Promise.all(promises).then(() => {
            this._loaded = true;
            
            this._tryStart();
        });
    }

    /**
     * @public
     */
    static setPlayerControlsEnabled(value) {
        Game._canControlPlayer = Boolean(value);
    }

    /**
     * @public
     */
    static canControlPlayer() {
        return !this.cutsceneIsActive() && !this._paused && Game._canControlPlayer;
    }

    /**
     * @public
     */
    static canPolitelyStartCutscene() {
        return !this._activeCutscene
            && (new Date().getTime() - Game._lastCutsceneStopTime) > this._CUTSCENE_SPACING_MS;
    }

    /**
     * @public
     */
    static clearRoom() {
        this._currentRoom = null;
    }

    /**
     * @public
     */
    static setRoom(room) {
        room.game = this;
        
        const currentRoom = this._currentRoom;

        return Room.load(room).then(room => {
            if (currentRoom) {
                currentRoom.tearDown();

                if (currentRoom.musicURI !== room.musicURI) {
                    JSAudio.stopFx(this._currentRoom.musicURI);
                }
            }
            
            if (this._player) room.addDrawable(this._player, 0);

            this._currentRoom = room;
            room.setup();            

            return room;
        });
    }

    /**
     * @private
     */
    static fadeOut() {
        Game._transitionEndStartTime = null;
        Game._transitionStartTime = new Date().getTime();

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, this._ROOM_TRANSITION_TIME_MS / 2);
        });
    }

    /**
     * @private
     */
    static fadeIn() {
        Game._transitionStartTime = null;
        Game._transitionEndStartTime = new Date().getTime();

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, this._ROOM_TRANSITION_TIME_MS / 2);
        });
    }

    /**
     * @public
     */
    static async transitionToRoom(room) {
        if (Game._transitionPromise) return Game._transitionPromise;

        await this.fadeOut();

        Game._nextRoom = room;

        ConfigurableController.lock();

        if (!isNaN(room)) {
            // It's an index.
            room = this.world.getRoom(room) || this.world.getRoom(0);
        }

        const setRoom = async () => {
            await this.setRoom(room);

            Game._nextRoom = null;
            Game._transitionPromise = null;

            ConfigurableController.unlock();
        };

        Game._transitionPromise = new Promise((resolve, reject) => {
            setRoom().then(() => {
                resolve(this._currentRoom);

                this.fadeIn();
            });
        });
        
        return Game._transitionPromise;
    }

    /**
     * @public
     */
    static beginCutscene(cutscene) {
        Game._activeCutscene = cutscene;
    }

    /**
     * @public
     */
    static endCutscene() {
        Game._lastCutsceneStopTime = new Date().getTime();
        Game._activeCutscene = null;
    }

    /**
     * @public
     */
    static advanceCutscene() {
        if (!Game._activeCutscene) return;

        Game._activeCutscene.advance();

        if (!Game._activeCutscene) return;

        if (Game._activeCutscene.isFinished()) {
            this.endCutscene();
        }
    }

    /**
     * @public
     */
    static skipCutscene() {
        if (!Game._activeCutscene) return;

        Game._activeCutscene.finish();

        this.endCutscene();
    }

    /**
     * @public
     */
    static cutsceneIsActive() {
        return Boolean(Game._activeCutscene);
    }

    /**
     * @public
     */
    static setPlayer(player) {
        Game._player = player;

        return null;
    }

    /**
     * @public
     */
    static getPlayer() {
        return Game._player;
    }

    /**
     * @public
     */
    static get currentRoom() {
        return this._currentRoom;
    }

    /**
     * @public
     */
    static get viewWidth() {
        return this._VIEW_WIDTH;
    }

    /**
     * @public
     */
    static get viewHeight() {
        return this._VIEW_HEIGHT;
    }

    /**
     * @public
     */
    static get viewX() {
        return Game._viewX;
    }

    /**
     * @public
     */
    static get viewY() {
        return Game._viewY;
    }

    /**
     * @public
     */
    static get canvas() {
        return this._canvas;
    }

    /**
     * @private
     */
    static _resizeCanvas() {
        if (this._canvas) {
            const w = this.viewWidth * this._scale;
            const h = this.viewHeight * this._scale;

            this._canvas.width = w;
            this._canvas.style.width = w + 'px';
            this._canvas.height = h;
            this._canvas.style.height = h + 'px';
        }
    }

    /**
     * @public
     */
    static set viewWidth(value) {
        this._VIEW_WIDTH = value;

        this._resizeCanvas();
    }

    /**
     * @public
     */
    static set viewHeight(value) {
        this._VIEW_HEIGHT = value;

        this._resizeCanvas();
    }

    /**
     * @public
     */
    static get paused() {
        return this._paused;
    }

    /**
     * @public
     */
    static pause(menu, byPassCutscene) {
        if (this._activeCutscene && !byPassCutscene) return;

        menu = menu || new PauseMenu();
        
        menu.game = this;
        menu.setup();

        this._activeCutscene = null;
        this._paused = true;
        this._activePauseMenu = menu;
    }

    /**
     * @public
     */
    static togglePause(menu) {
        if (this._paused) this.unpause();
        else this.pause(menu);
    }

    /**
     * @public
     */
    static clearPauseMenu() {
        if (this._activePauseMenu) this._activePauseMenu.tearDown();

        this._activePauseMenu = null;
    }

    /**
     * @public
     */
    static unpause() {
        this._paused = false;

        this.clearPauseMenu();
    }

    /**
     * @public
     */
    static update() {
        if (!this._currentRoom) return;

        if (this.canControlPlayer()) {
            this._currentRoom.update();

            if (this._player)
                this._player.update();
        }

        ConfigurableController.update();

        Editor.update();
    }

    /**
     * @private
     */
    static _handleRoomTransition() {
        let a;

        if (Game._transitionStartTime) {
            a = (new Date().getTime() - Game._transitionStartTime) / (Game._ROOM_TRANSITION_TIME_MS / 2);
            a = Math.sin(Math.min(a, 1.0) * Math.PI / 2);
        } else if (Game._transitionEndStartTime) {

            a = (new Date().getTime() - Game._transitionEndStartTime) / (Game._ROOM_TRANSITION_TIME_MS / 2);
            a = 1 - Math.sin(Math.min(a, 1.0) * Math.PI / 2); 

            if (a <= 0) Game._transitionEndStartTime = null;
        }

        if (a) {
            this._graphics.noStroke();
            this._graphics.fill(0, 0, 0, a);
            this._graphics.rect(0, 0, this.viewWidth, this.viewHeight);
        }
    }

    /**
     * @private
     * @abstract
     */
    static _afterDrawRoom() {}

    /**
     * @private
     * @abstract
     */
    static _drawOverlay() {}

    /**
     * @public
     */
    static setViewOffset(x, y) {
        Game._viewOffsetX = x;
        Game._viewOffsetY = y;
    }

    /**
     * @public
     */
    static draw(timestamp) {

        window.requestAnimationFrame(() => {
            this.draw();
        });

        if (timestamp - this._animationStart < (1000 / this._frameRate)) {
            // Don't render yet.
            return;
        } else {
            this._animationStart = timestamp;
        }

        this._graphics.push();
        this.update();
        this._graphics.scale(this._scale);

        if (this._currentRoom) {
            let viewX = 0;
            let viewY = 0;

            this._currentRoom.drawBackground(viewX - this._currentRoom.xMin);

            this._graphics.push();

            this._viewX = viewX + this._viewOffsetX;
            this._viewY = viewY + this._viewOffsetY;

            this._graphics.translate(-this._viewX, -this._viewY);
            
            this._currentRoom.draw();

            this._afterDrawRoom();
            
            this._graphics.pop();

            this._drawOverlay();

            if (this._player && !this._activeCutscene)
                this._player.drawHUD();

            if (this.debugMode) {
                this._graphics.noStroke();
                this._graphics.fill('#FFF');
                this._graphics.textAlign(Shapes.ALIGN.LEFT, Shapes.ALIGN.CENTER);
                this._graphics.text(`Layer: ${Editor.currentLayer}`, 4, 12);
            }
        }

        if (this._activeCutscene) {
            this._activeCutscene.draw();

            if (this._activeCutscene.isFinished()) this.endCutscene();
        }

        this._handleRoomTransition();

        this._graphics.pop();
    }

    /**
     * @private
     */
    static getMousePosOnScreen() {
        const pos = Mouse.getPositionRelativeToElement(null, this._canvas);
        let x, y;

        x = pos.x / this.scale;
        y = pos.y / this.scale;

        return {x, y};
    }

    /**
     * @public
     */
    static mouseIsInsideGameCanvas() {
        const pos = this.getMousePosOnScreen();

        return (pos.x >= 0 && pos.y >= 0
            && pos.x < this.viewWidth && pos.y < this.viewHeight);
    }

    /**
     * @private
     */
    static getMousePosInGame() {
        let result = this.getMousePosOnScreen();

        result.x += this.viewX + this._viewOffsetX;
        result.y += this.viewY + this._viewOffsetY;

        return result;
    }

    /**
     * @public
     */
    static get scale() {
        return Game._scale;
    }

    /**
     * @public
     */
    static set scale(val) {
        Game._scale = val;

        this._resizeCanvas();
    }

    /**
     * @public
     */
    static onInit(callback) {
        this._initCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    static initControls() {
        ConfigurableController
            .setInput(
                KEYS.ARROW_UP,
                new KeyDownBooleanInput(KEYS.ARROW_UP).or(
                    new KeyDownBooleanInput(KEYS.W_KEY)
                ),
            )
            .setInput(
                KEYS.ARROW_RIGHT,
                new KeyDownBooleanInput(KEYS.ARROW_RIGHT).or(
                    new KeyDownBooleanInput(KEYS.D_KEY)
                ),
            )
            .setInput(
                KEYS.ARROW_DOWN,
                new KeyDownBooleanInput(KEYS.ARROW_DOWN).or(
                    new KeyDownBooleanInput(KEYS.S_KEY)
                ),
            )
            .setInput(
                KEYS.ARROW_LEFT,
                new KeyDownBooleanInput(KEYS.ARROW_LEFT).or(
                    new KeyDownBooleanInput(KEYS.A_KEY)
                ),
            )
            .setInput(
                KEYS.ENTER_KEY,
                new KeyDownBooleanInput(KEYS.ENTER_KEY).or(
                    new KeyDownBooleanInput(KEYS.W_KEY)
                ),
            )
            .setInput(
                KEYS.ESCAPE_KEY,
                new KeyDownBooleanInput(KEYS.ESCAPE_KEY),
            )
        ;
    }

    /**
     * @private
     */
    static _fixCanvasDensity() {
        let canvas = this._canvas;
        const width = canvas.clientWidth;
        const height = canvas.clientHeight;

        if (canvas.width !== width || canvas.height !== height) {
            canvas.width = width;
            canvas.height = height;
        }
    }

    /**
     * @public
     */
    static get context() {
        return this._context
    }

    /**
     * @public
     */
    static parent(id) {
        // this._p5Canvas.parent(id);
    }

    /**
     * @public
     */
    static get graphics() {
        return this._graphics;
    }

    /**
     * @public
     */
    static init(containerId, onInit) {
        if (!containerId) {
            console.error('No container ID provided to init()');
        }
        
        this.initControls();

        this._started = false;
        this._loaded = false;
        this._userHasInteracted = false;
        
        this._VIEW_WIDTH = 600;
        this._VIEW_HEIGHT = 400;

        if (typeof DebugToolbarComponent !== 'undefined') {
            document.getElementById(containerId).innerHTML = new DebugToolbarComponent().render();
        }
        
        const w = this.viewWidth * Game._scale;
        const h = this.viewHeight * Game._scale;

        this._canvas = document.createElement('canvas');
        this._canvas.width = w;
        this._canvas.height = h;

        document.getElementById(containerId).appendChild(this._canvas);

        this._context = this._canvas.getContext('2d');
        this._graphics = new Shapes(this._context);

        this._canvas.oncontextmenu = event => {
            event.preventDefault();

            if (Mouse.isRightClick(event)) {
                Editor.handleCanvasRightClick();
            }
        };

        this._canvas.onclick = event => {
            if (!Mouse.isRightClick(event)) {
                Editor.handleCanvasLeftClick();
            } else {
                Editor.handleCanvasRightClick();
            }
        };

        // this._fixCanvasDensity();

        Mouse.onRelease(() => {
            if (!this._activePauseMenu) return;

            this._activePauseMenu._handleClick();
        });

        Editor.init();
        this._initCallbacks.forEach(callback => callback());

        const touch = new Touch(this.canvas, true);

        Handle.initTouch(touch);

        this._animationStart = new Date().getTime();
        
        if (!this._currentRoom) {
            this._currentRoom = new Room();
            this._currentRoom.game = this;
        }
        
        if (onInit) onInit();

        this.draw();
    }

    /**
     * @public
     */
    static get world() {
        return this._world;
    }

    /**
     * @public
     */
    static set world(value) {
        this._world = value;
    }

    /**
     * @public
     */
    static set GRID_SIZE_PX(value) {
        Game._gridSizePx = value;
    }

    /**
     * @public
     */
    static get GRID_SIZE_PX() {
        return Game._gridSizePx;
    }

    /**
     * @private
     */
    static _handleMouseScroll(dir) {
        if (this._activePauseMenu) {
            this._activePauseMenu.scroll(dir);
        }
    }
}

Mouse.onScroll(dir => {
    Game._handleMouseScroll(dir);
});

/**
 * Whether to show debugging information.
 * @type {Boolean}
 */
Game.debugMode = false;

/**
 * The approximate frame rate of the game.
 * @type {Number}
 */
Game.frameRate = 30;

/**
 * All CollisionGroups for use in the game.
 * @type {Object<CollisionGroup>}
 */
Game.COLLISION = {
    TILES: new CollisionGroup(),
};

Game._gridSizePx = 16;
Game._activePauseMenu = null;
Game._paused = false;
Game._scale = 1;
Game._canvas = null;
Game._player = null;
Game._activeCutscene = null;
Game._nextRoom = null;
Game._transitionStartTime = null;
Game._ROOM_TRANSITION_TIME_MS = 1000;
Game._viewX = 0;
Game._viewY = 0;
Game._scale = 1;
Game._transitionPromise = null;
Game._initCallbacks = [];
Game._canControlPlayer = true;
Game._world = null;
Game._viewOffsetX = 0;
Game._viewOffsetY = 0;
Game._lastCutsceneStopTime = 0;
Game._CUTSCENE_SPACING_MS = 200;
Game._CENTER_VIEW_ON_PLAYER = false;
Game._AUDIO = {};
