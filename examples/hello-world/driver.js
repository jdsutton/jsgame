import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');
import('MyGame.js');

/**
 * @class
 */
class HelloWorldDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();

        MyGame.init(ID.CONTAINER);

        MyGame.load().then(() => {
            // Do game things.
        });
    }
}

Page.addLoadEvent(() => {
    HelloWorldDriver.init();
});

const ID = {
    CONTAINER: 'container',
};

const CLASS = {

};
