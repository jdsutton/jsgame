import('JSImage.js');

/**
 * @class
 */
class Background {

    /**
     * @constructor
     */
    constructor(uris) {
        this._layerURIs = uris;
        this._layers = [];
        this._parallax = false;

        this._loadPromise = JSImage.loadAllImages(uris, (img, key) => {
            this._layers[key] = img;
        });
    }

    /**
     * @public
     */
    get uris() {
        return this._layerURIs;
    }

    /**
     * @public
     */
    load() {
        return this._loadPromise;
    }

    /**
     * @public
     */
    draw(xOff, k) {

        if (this._parallax) {
            this._drawParallax(xOff, k);

            return;
        }

        this._layers.forEach(img => {
            // const wr = img.width / this.game.viewWidth;
            // const hr = img.height / this.game.viewHeight;

            this.game.graphics.drawImage(img, 0, 0);
        });
    }

    /**
     * @private
     */
    _drawParallax(xOff, k) {
        xOff = xOff || 0;
        k = k || 0.1;
        let layer = 0;
        let xStart = 0;
        // let width = Game.viewWidth;
        
        // if (Game.currentRoom && !Game.paused) {
        //     width = Game.currentRoom.width;
        //     xStart = Game.currentRoom.xMin;
        // }

        const xEnd = this.game.viewWidth;
        const yEnd = this.game.viewHeight;

        this._layers.forEach(img => {
            let x = xStart;

            while (true) {
                const xPos = x - (xOff * layer * k);

                if (xPos > xEnd) {
                    break;
                }

                this.game.graphics.drawImage(img, xPos, 0, xEnd, yEnd);

                x += img.width;
            }

            layer++;
        });
    }
}