import('/src/JSUtils/datastructures/QuadTree.js');

class CollisionGroup {

    constructor() {
        this.members = new Set();

        // FIXME.
        this._index = new QuadTree({
            x: 0,
            y: 0,
            width: 2000,
            height: 2000,
        });
    }

    /**
     * @private
     */
    static _collides(a, b) {
        return (a.x < b.x + b.width
            && a.x + a.width > b.x
            && a.y < b.y + b.height
            && a.y + a.height > b.y);
    }

    /**
     * @public
     */
    collidesWith(rect) {
        const possibleCollisions = this._index.retrieve(rect);
        let result = [];

        for (let r of possibleCollisions) {
            if (CollisionGroup._collides(rect, r)) {
                result.push(r);
            }
        }

        return result;
    }

    add(x) {
        this.members.add(x);
        this._index.insert(x);

        return this;
    }

    delete(x) {
        this.members.delete(x);
        this._index.delete(x);

        return this;
    }

    has(x) {
        return this.members.has(x);
    }
}