import('UniqueId.js');

/**
 * @class
 */
class WebRTCClientFactory {

    /**
     * constructor
     */
    constructor(clientClass) {
        this._clientClass = clientClass;
        this._sessionId = UniqueId.get();
    }

    /**
     * @public
     */
    produce() {
        return new this._clientClass(UniqueId.get(), this._sessionId);
    }
}

WebRTCClientFactory._CLIENT_ID_KEY = 'WebRTCClientFactory_clientId';
