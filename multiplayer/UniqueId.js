
/**
 * @class
 */
class UniqueId {
	
    /**
     * @public
     */
    static get() {
        return new Date().getTime() + '' + Math.random();
    }
}