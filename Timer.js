/**
 * @class
 */
class Timer {

    /**
     * @constructor
     */
    constructor(fireRateMs) {
        this._startTimeMs = 0;
        this._fireRateMs = fireRateMs;
    }

    /**
     * @public
     */
    set() {
        this._startTimeMs = new Date().getTime();
    
        return this;
    }

    /**
     * @public
     */
    canFire() {
        return new Date().getTime() - this._startTimeMs > this._fireRateMs;
    }
}