
/**
 * @class
 */
class PathFinder {
    
    /**
     * @constructor
     */
    constructor(room, avoidClasses, navWidth, navHeight, nodeSpacingPx) {
        this._room = room;
        this._avoidClasses = avoidClasses;
        this._navWidth = navWidth || 32;
        this._navHeight = navHeight || 32;
        this._nodeSpacingPx = nodeSpacingPx || 16;
        this._nodes = [];

        // Lay out nodes.
    }

    /**
     * @public
     */
    findPath(navProp, target) {
        // Find start and end.
        
        // Do A*.
        let path = this._aStar(start, end);
        // Add nodes.

        // Return path.
    }

    /**
     * @public
     */
    getPointOnPath(path, startTime, rate) {

    }
}
