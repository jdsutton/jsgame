import('/src/JSUtils/util/File.js');
import('/src/JSUtils/util/Request.js');

/**
 * @class
 */
class WorldManager {

    /**
     * @private
     */
    static _setWorldFromFileContents(contents, name) {
        let world = World.fromString(contents);

        if (name) world.name = name;

        Game.world = world;
    }

    /**
     * @public
     */
    static saveToLocalSystem(game) {
        const world = Game.world;
        const json = world.toString();
        const filename = world.name + '.world';

        File.save(json, filename);
    }

    /**
     * @public
     */
    static saveToDropbox() {
        const world = Game.world;
        const json = world.toString();
        const filename = world.name + '.world';
        const url = File.createDataURL(json);

        Dropbox.save(url, filename, {
            error: err => console.error(err),
        });
    }

    /**
     * @public
     */
    static loadFromDropbox() {
        return new Promise((resolve, reject) => {
            Dropbox.choose({
                success: files => {
                    const uri = files[0].link;

                    Request.get(uri).then(contents => {
                        this._setWorldFromFileContents(contents,
                            files[0].name.replace('.world', ''));

                            resolve(Game.world);
                    });
                },
                cancel: reject,
                multiselect: false,
                extensions: ['.world'],
                folderselect: false,
                linkType: 'direct',
            });
        });
    }

    /**
     * @public
     */
    static loadFromLocalSystem() {
        return new Promise((resolve, reject) => {
            File.select('text', ['.world']).then(contents => {
                this._setWorldFromFileContents(contents);

                resolve(Game.world);
            }).catch(e => {
                console.log(e);
                reject(e);
            });
        });
    }

    /**
     * @public
     */
    static loadFromURI(uri) {
        if (!this._loadPromisesByURI[uri]) {
            this._loadPromisesByURI[uri] = new Promise((resolve, reject) => {
                Request.get(uri).then(contents => {
                    this._setWorldFromFileContents(contents);
                    
                    resolve(Game.world);
                });
            });
        }

        return this._loadPromisesByURI[uri];
    }
}

WorldManager._loadPromisesByURI = {};
