/**
 * @class
 */
class World {

    /**
     * @constructor
     */
    constructor(name) {
        this.name = name || 'New World';
        this._rooms = [];
        this._roomsJSON = [];
        this._roomNames = [];
    }

    /**
     * public
     */
    getRoomNames() {
        return this._roomNames.slice();
    }

    /**
     * @public
     */
    setRoomName(i, name) {
        // TODO: Update current room if needed.

        this._roomNames[i] = name;
    }

    /**
     * @public
     */
    getNameOfRoom(i) {
        if (!this._roomNames[i]) {
            this._roomNames[i] = 'New Room ' + i;
        }

        return this._roomNames[i];
    }

    /**
     * @public
     */
    numRooms() {
        return Math.max(this._rooms.length, this._roomsJSON.length);
    }

    /**
     * @public
     */
    getRoom(i) {
        if (!this._rooms[i]) {
            const json = this._roomsJSON[i];

            try {
                this._rooms[i] = Room.fromJSON(json);
            } catch (e) {
                console.error('Could not parse room ' + i);
                throw e;
            }
        }

        let result = this._rooms[i];

        result.name = this.getNameOfRoom(i);

        return result;
    }

    /**
     * @public
     */
    preload(roomIndex) {
        // TODO.
    }

    /**
     * @public
     */
    getRoomByName(name) {
        const index = this._roomNames.indexOf(name);

        if (index < 0) return null;

        return this.getRoom(index);
    } 

    /**
     * @public
     */
    indexOfRoom(room) {
        return this._roomNames.indexOf(room.name);
    }

    /**
     * @public
     */
    addRoom(room) {
        if (this._rooms.find(r => r && r.name === room.name)) return this;

        if (!room.name) {
            room.name = 'New Room ' + this._rooms.length;
        }

        const index = Math.max(this._rooms.length, this._roomsJSON.length);

        this._rooms[index] = room;
        this._roomNames[index] = room.name;

        return this;
    }

    /**
     * @public
     */
    removeRoom(name) {
        this._rooms = this._rooms.filter(
            r.name !== name);

        return this;
    }

    /**
     * @public
     */
    toJSON() {
        const rooms = this._rooms.map(r => r.toJSON());

        return '{"_rooms":[' + rooms.join(',') + ']}';
    }

    /**
     * @public
     */
    static fromJSON(j) {
        let result = Object.assign(new World(), window.JSON.parse(j));

        result._rooms = result._rooms.map(r => Room.fromDict(r));
        result._rooms = Array.from(result._rooms);

        return result;
    }

    /**
     * @public
     */
    toString() {
        let result = '_roomNames:' + JSON.stringify(this._roomNames) + '\n';
        let rooms = [];

        for (let i = 0; i < Math.max(this._rooms.length, this._roomsJSON.length); i++) {
            if (this._rooms[i]) {
                rooms[i] = this._rooms[i].toJSON();
            } else {
                rooms[i] = this._roomsJSON[i];
            }
        }

        console.assert(rooms.length === Math.max(this._rooms.length, this._roomsJSON.length));
        console.assert(rooms.length >= this._roomNames.length);

        return result + rooms.join('\n');
    }

    /**
     * @public
     */
    static fromString(s) {
        let result = new World();
        let lines = s.split('\n');
        let i = 0;

        for (let line of lines) {
            if (line.startsWith('//')) continue;
            if (!line) continue;
            
            if (line.startsWith('_roomNames:')) {
                const names = line.split(':')[1];

                result._roomNames = JSON.parse(names);
            } else {
                if (!result._roomNames[i]) result._roomNames[i] = 'New Room ' + i;

                result._roomsJSON.push(line);
            }

            i++;
        }
        return result;
    }    
}