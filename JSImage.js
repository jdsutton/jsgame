/**
 * @class
 */
class JSImage {

    /**
     * @public
     */
    static load(uri) {
        const cached = this._cachedImages[uri];

        if (cached) {
            return Promise.resolve(cached);
        }

        return new Promise((resolve, reject) => {
            let img = document.createElement('img');
            
            img.addEventListener('load', () => {
                if (!this._cachedImages[uri]) {
                    this._cachedImages[uri] = img;
                }

                resolve(img);
            });

            img.addEventListener('error', () => {
                reject();
            });

            img.src = uri;

        }).catch((e) => {
            console.error('Failed to load image: ' + uri);
            console.error(e);
        });
    }
    
    /**
     * @public
     */
    static loadAllImages(uris, imgCallback) {
        let promises = [];
        let index = 0;

        imgCallback = imgCallback || (() => {});

        uris.forEach(uri => {
            const i = index;
            const p = this.load(uri).then((img) => {
                if (imgCallback)
                    imgCallback(img, i);
            });

            promises.push(p);

            index++;
        });

        return Promise.all(promises);
    }

    /**
     * @public
     */
    static crop(image, x, y, width, height) {
        if (typeof image === 'string') image = this.get(image);

        let newFrame = new Image(width, height);
        const canvas = document.createElement('canvas');
        canvas.width = width;
        canvas.height = height;
        
        const ctx = canvas.getContext('2d');

        ctx.drawImage(image, x, y, width, height, 0, 0, width, height);

        newFrame.src = canvas.toDataURL();
    
        return newFrame;
    }

    /**
     * @public
     */
    static filter(image, filter, filterParam) {
        let newFrame = new Image(image.width, image.height);
        
        newFrame.copy(image, 0, 0, image.width, image.height, 0, 0, image.width, image.height);
        newFrame.filter(filter, filterParam);

        return newFrame;
    }

    /**
     * @public
     */
    static get(uri) {
        return this._cachedImages[uri];
    }

    /**
     * @public
     * @param {HTMLImageElement} img
     */
    static getBoundingRectIgnoreTransparent(img) {
        let canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        canvas.getContext('2d', {willReadFrequently: true}).drawImage(img, 0, 0, img.width, img.height);
        const context = canvas.getContext('2d');

        let x = 0;
        let y = 0;
        let w = 0;
        let h = 0;

        for (x = 0; x < img.naturalWidth; x++) {
            let pixelFound = false;

            for (let yy = 0; yy < img.naturalHeight; yy++) {
                const data = context.getImageData(x, yy, 1, 1).data;

                if (data[3] > 0) {
                    pixelFound = true;
                    break;
                }

            }

            if (pixelFound) break;
        }

        for (y = 0; y < img.naturalHeight; y++) {
            let pixelFound = false;

            for (let xx = 0; xx < img.naturalWidth; xx++) {
                const data = context.getImageData(xx, y, 1, 1).data;

                if (data[3] > 0) {
                    pixelFound = true;
                    break;
                }

            }

            if (pixelFound) break;
        }

        for (let xx = x; xx < img.naturalWidth; xx++) {
            let pixelFound = false;

            for (let yy = 0; yy < img.naturalHeight; yy++) {
                const data = context.getImageData(xx, yy, 1, 1).data;

                if (data[3] > 0) {
                    pixelFound = true;
                    break;
                }

            }

            if (!pixelFound) break;

            w++;
        }

        for (let yy = y; yy < img.naturalHeight; yy++) {
            let pixelFound = false;

            for (let xx = 0; xx < img.naturalHeight; xx++) {
                const data = context.getImageData(xx, yy, 1, 1).data;

                if (data[3] > 0) {
                    pixelFound = true;
                    break;
                }

            }

            if (!pixelFound) break;

            h++;
        }

        return {x, y, width: w, height: h};
    }
}

JSImage._cachedImages = {};
