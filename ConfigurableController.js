import('/src/JSUtils/io/JSGamepad.js');
import('/src/JSUtils/io/Keys.js');
import('/src/JSUtils/io/Mouse.js');

/**
 * @class
 */
class ConfigurableController {

    /**
     * @public
     */
    static lock() {
        this._locked = true;

        return this;
    }

    /**
     * @public
     */
    static unlock() {
        this._locked = false;

        return this;
    }

    /**
     * public
     */
    static release(name) {
        const callbacks = this._inputReleasedCallbacksByName[name];
        this._inputsReleased[name] = true;

        if (callbacks) {
            callbacks.forEach(callback => callback());
        }
    }

    /**
     * @public
     */
    static update() {
        if (this._locked) return;

        this._inputsReleased = {};

        const names = Array.from(Object.keys(this._inputs));

        names.forEach(name => {
            const state = this.getInput(name);
            let callbacks;

            if (state && !this._prevInputStatesByName[name]) {
                callbacks = this._inputPressedCallbacksByName[name];
            } else if (state) {
                callbacks = this._ifInputDownCallbacksByName[name];
            }else if (!state && this._prevInputStatesByName[name]) {
                callbacks = this._inputReleasedCallbacksByName[name];
                this._inputsReleased[name] = true;
            }

            if (callbacks) callbacks.forEach(c => c());

            this._prevInputStatesByName[name] = state;
        });
    }

    /**
     * @public
     */
    static onInputPressed(name, callback, clear) {
        if (!name) {
            throw new Error('Not a valid input name: ' + name);
        }
        
        if (!this._inputPressedCallbacksByName[name] || clear)
            this._inputPressedCallbacksByName[name] = [];

        this._inputPressedCallbacksByName[name].push(callback);

        return this;
    }

    /**
     * @public
     */
    static ifInputDown(name, callback, clear) {
        if (!name) {
            throw new Error('Not a valid input name: ' + name);
        }
        
        if (!this._ifInputDownCallbacksByName[name] || clear)
            this._ifInputDownCallbacksByName[name] = [];

        this._ifInputDownCallbacksByName[name].push(callback);

        return this;
    }

    /**
     * @public
     */
    static onInputReleased(name, callback, clear) {
        if (!name) {
            throw new Error('Not a valid input name: ' + name);
        }
        
        if (!this._inputReleasedCallbacksByName[name] || clear)
            this._inputReleasedCallbacksByName[name] = [];

        this._inputReleasedCallbacksByName[name].push(callback);

        return this;
    }

    /**
     * @static
     */
    static setInput(name, booleanInput) {
        this._inputs[name] = booleanInput;

        return this;
    }

    /**
     * @static
     */
    static getInputLabel(name) {
        if (name === this._mapInputKey)
            return '<press any button>';

        const input = this._inputs[name];

        if (!input) return 'Not set';

        return input.getLabel();
    }

    /**
     * @static
     */
    static getInput(name) {
        if (this._locked) return this._prevInputStatesByName[name];

        if (!this._inputs[name]) return false;

        return this._inputs[name].isPressed();
    }

    /**
     * @static
     */
    static getInputReleased(name) {
        if (this._locked) return this._prevInputStatesByName[name];

        return Boolean(this._inputsReleased[name]);
    }

    /**
     * @public
     */
    static mapInput(inputKey) {
        this._mapInputKey = inputKey;
        this._mapInputKeyTime = new Date().getTime();
        this._scanGamepadInputInterval = setInterval(() => {
            this._scanGamepads();
        }, 100);

        return new Promise((resolve, reject) => {
            this._mapResolver = resolve;
        });
    }

    /**
     * @public
     */
    static isMappingInput() {
        return Boolean(this._mapInputKey);
    }

    /**
     * @private
     */
    static _scanGamepads() {
        const gamepads = JSGamepad.list();

        for (let g = 0; g < gamepads.length; g++) {
            const gamepad = gamepads[g];
            if (!gamepad) return;

            for (let i = 0; i < gamepad.axes.length; i++) {
                const axis = gamepad.axes[i];

                if (Math.abs(axis) > 0.5) {
                    this._mapGamepadAxis(g, i, Math.sign(axis));
                    
                    return;
                }
            }

            for (let i = 0; i < gamepad.buttons.length; i++) {
                if (gamepad.buttons[i].pressed) {
                    this._mapGamepadButton(g, i);

                    return;
                }
            }
        }
    }

    /**
     * @private
     */
    static _mapGamepadAxis(gamepad, axis, sign) {
        if ((new Date().getTime() - this._mapInputKeyTime) < this._INPUT_MAP_DELAY_MS) return;

        this._inputs[this._mapInputKey] = new GamepadAxisBooleanInput(gamepad, axis, sign);
        this._mapInputKey = null;
        clearInterval(this._scanGamepadInputInterval);
        this._mapResolver();
    }

    /**
     * @private
     */
    static _resolveKeyMapping() {
        this._mapInputKey = null;
        clearInterval(this._scanGamepadInputInterval);
        this._mapResolver();
    }

    /**
     * @private
     */
    static _mapGamepadButton(gamepad, button) {
        if ((new Date().getTime() - this._mapInputKeyTime) < this._INPUT_MAP_DELAY_MS) return;

        this._inputs[this._mapInputKey] = new GamepadButtonBooleanInput(gamepad, button);
        
        this._resolveKeyMapping();
    }

    /**
     * @private
     */
    static _mapKeyTo(key) {
        if ((new Date().getTime() - this._mapInputKeyTime) < this._INPUT_MAP_DELAY_MS) return;

        this._inputs[this._mapInputKey] = new KeyDownBooleanInput(key);
        
        this._resolveKeyMapping();
    }

    /**
     * @private
     */
    static _mapMouseButton(button) {
        if ((new Date().getTime() - this._mapInputKeyTime) < this._INPUT_MAP_DELAY_MS) return;

        this._inputs[this._mapInputKey] = new MouseButtonBooleanInput(button);
        
        this._resolveKeyMapping();
    }

    /**
     * @public
     */
    static load() {
        const s = localStorage.getItem(this._DATA_KEY);
        const data = JSON.parse(s);

        if (!data || !data.inputs) return;

        for (let key in data.inputs) {
            try {
                const input = data.inputs[key];

                this._inputs[key] = BooleanInput.fromDict(input);
            } catch (e) {
                console.warn(e);
            }
        }

        return this;
    }

    /**
     * @public
     */
    static save() {
        let inputs = {};

        for (let key in this._inputs) {
            inputs[key] = this._inputs[key].toDict();
        }

        const data = {
            inputs,
        };

        localStorage.setItem(this._DATA_KEY, JSON.stringify(data));
    }
}

/**
 * @class
 */
class BooleanInput {

    /**
     * @constructor
     */
    constructor() {
        this._nextInput = null;
    }

    /**
     * @public
     */
    isPressed() {
        if (this._nextInput && !this._nextInput.isPressed) console.log(this);
        return this._nextInput && this._nextInput.isPressed();
    }

    /**
     * @public
     */
    or(booleanInput) {
        this._nextInput = booleanInput;

        return this;
    }

    /**
     * @public
     */
    toDict() {
        let result = Object.assign({}, this);

        result.class = this.constructor.name;

        if (this._nextInput) {
            result._nextInput = this._nextInput.toDict();
        }

        return result;
    }

    /**
     * @public
     */
    static fromDict(d) {
        if (!d.class) return null;

        let result = new (eval(d.class))();
        delete d.class;

        Object.assign(result, d);

        if (d._nextInput) {
            result._nextInput = this.fromDict(d._nextInput);
        }

        return result;
    }
};

class KeyDownBooleanInput extends BooleanInput {

    /**
     * @constructor
     */
    constructor(key) {
        super(...arguments);

        this._key = key;
    }

    /**
     * @public
     */
    isPressed() {
        if (super.isPressed()) return true;

        return Key.isDown(this._key);
    }

    /**
     * @public
     */
    getLabel() {
        let result = Key.getName(this._key);

        if (this._nextInput) {
            result = result + ' OR ' + this._nextInput.getLabel();
        }

        return result;
    }
}

class MouseButtonBooleanInput extends BooleanInput {

    /**
     * @constructor
     */
    constructor(button, game) {
        super(...arguments);

        this._button = button;
        this.game = game;
    }

    /**
     * @public
     */
    isPressed() {
        if (super.isPressed()) return true;

        return Mouse.buttonIsPressed(this._button)
            && !Editor.hasPlaceable()
            && this.game.mouseIsInsideGameCanvas();
    }

    /**
     * @public
     */
    getLabel() {
        let result = 'Mouse button ' + this._button;

        if (this._nextInput) {
            result = result + ' OR ' + this._nextInput.getLabel();
        }

        return result;
    }
}

class GamepadAxisBooleanInput extends BooleanInput {

    /**
     * @constructor
     */
    constructor(gamepad, axis, sign) {
        super(...arguments);

        this._gamepad = gamepad;
        this._axis = axis;
        this._sign = sign;
    }

    /**
     * @public
     */
    isPressed() {
        if (super.isPressed()) return true;

        const gamepad = JSGamepad.list()[this._gamepad];

        if (!gamepad) return;

        return gamepad.axes[this._axis] * this._sign > 0.5;
    }

    /**
     * @public
     */
    getLabel() {
        let result = `Gamepad Axis (${this._axis}, ${this._sign})`;

        if (this._nextInput) {
            result = result + ' OR ' + this._nextInput.getLabel();
        }

        return result;
    }
}

class GamepadButtonBooleanInput extends BooleanInput {

    /**
     * @constructor
     */
    constructor(gamepad, button) {
        super(...arguments);

        this._gamepad = gamepad;
        this._button = button;
    }

    /**
     * @public
     */
    isPressed() {
        if (super.isPressed()) return true;

        const gamepad = JSGamepad.list()[this._gamepad];

        if (!gamepad) return false;

        const button = gamepad.buttons[this._button];

        if (!button) return false;

        return button.pressed;
    }
    
    /**
     * @public
     */
    getLabel() {
        let result = `Gamepad Button ${this._button}`;

        if (this._nextInput) {
            result = result + ' OR ' + this._nextInput.getLabel();
        }

        return result;
    }
}

ConfigurableController._inputPressedCallbacksByName = {};
ConfigurableController._inputReleasedCallbacksByName = {};
ConfigurableController._ifInputDownCallbacksByName = {};
ConfigurableController._prevInputStatesByName = {};
ConfigurableController._mapInputKey = null;
ConfigurableController._mapInputKeyTime = null;
ConfigurableController._mapResolver = null;
ConfigurableController._scanGamepadInputInterval = null;
ConfigurableController._inputs = {};
ConfigurableController._DATA_KEY = 'ConfigurableController.state';
ConfigurableController._INPUT_MAP_DELAY_MS = 250;
ConfigurableController._inputsReleased = {};
ConfigurableController._locked = false;

Key.onKeyReleased(KEYS.ANY_KEY, key => {
    if (ConfigurableController._mapInputKey) {
        ConfigurableController._mapKeyTo(key);
    }
});

Mouse.onRelease(event => {
    if (ConfigurableController._mapInputKey) {
        ConfigurableController._mapMouseButton(event.button);
    }
});