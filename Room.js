import('Animation.js');
import('Background.js');
import('Enemy.js');
import('JSAudio.js');
import('JSImage.js');
import('Projectile.js');
import('Tile.js');
import('/src/JSUtils/util/Datetime.js');
import('/src/JSUtils/util/Request.js');

/**
 * @class
 */
class Room {

    /**
     * @constructor
     */
    constructor() {
        this.x = 0;
        this.y = 0;
        this._tiles = [];
        this._projectiles = [];
        this._enemies = [];
        this._props = [];
        this.xMin = 0;
        this.xMax = 0;
        this.yMin = 0;
        this.yMax = 0;
        this.width = 0;
        this.height = 0;
        this._musicURI = null;
        this._musicLoadPromise = null;
        this._layers = {};
        this._background = null;
        this._fgLayers = [];
        this._fgLayerURIs = [];
        this._fgLoadPromise = null;
        this._loadPromise = null;
        this._minLayer = 0;
        this._maxLayer = 0;
        this.uri = null;
        this._darknessLevel = 0;
        this._darknessOverlay = null;
        this._lightSources = [];
        this.musicAudio = null;
        this.name = 'New Room ' + Datetime.dateToDateTimeString(new Date());
        this.game = null;
    }

    /**
     * @public
     */
    set background(v) {
        v.game = this.game;

        this._background = v;
    }

    /**
     * @public
     */
    get background() {
        return this._background;
    }

    /**
     * @public
     */
    setBackgroundLayers(uris) {
        this._background = new Background(uris);
        this._background.game = this.game;

        return this;
    }

    /**
     * @public
     */
    setForegroundLayers(uris) {
        this._fgLayerURIs = uris;

        this._fgPromise = JSImage.loadAllImages(uris, (img, key) => {
            this._fgLayers[key] = img;
        });

        return this;
    }

    /**
     * @public
     */
    setMusic(uri, volume) {
        this._musicLoadPromise = JSAudio.load(uri).then(audio => {
            this.musicAudio = audio;
            audio.loop = true;

            if (this._musicURI !== uri) {
                JSAudio.stopFx(this._musicURI);
                this._musicURI = uri;
            }

            JSAudio.playFx(uri, false, volume);
        });

        return this;
    }

    /**
     * @public
     */
    setup() {
        if (this._musicURI)
            this.setMusic(this._musicURI);

        if (this._background && this._background.setup)
            this._background.setup();

        Array.from(this.drawables).forEach(d => {
            // Preload neighbors.
            if (d && d.destinationRoomIndex) {
                Game.world.preload(d.destinationRoomIndex);
            }

            if (d) d.setup();
        });

        return this;
    }

    /**
     * @public
     */
    get musicURI() {
        return this._musicURI;
    }

    /**
     * @public
     */
    addProjectile(projectile) {
        this.addDrawable(projectile);
    }

    /**
     * @public
     */
    addParticle(particle) {
        this.addDrawable(particle);
    }

    /**
     * @public
     */
    hasTileAtPosition(layer, x, y) {
        for (let tile of this._tiles) {
            if (tile.defaultLayer === layer
                && tile.x === x
                && tile.y === y) {
                
                return tile;
            }
        }

        return false;
    }

    /**
     * @public
     */
    addTile(tile) {
        this._tiles.forEach(tile2 => {
            if (tile2.defaultLayer === tile.defaultLayer
                && tile2.x === tile.x
                && tile2.y === tile.y) {
                
                this.removeDrawable(tile2);
            }
        });

        this._tiles.push(tile);

        if (tile.isSolid()) {
            this.xMin = Math.min(this.xMin, tile.x);
            this.yMin = Math.min(this.yMin, tile.y);
            this.xMax = Math.max(this.xMax, tile.x + tile.width);
            this.yMax = Math.max(this.yMax, tile.y + tile.height);

            this.width = this.xMax - this.xMin;
            this.height = this.yMax - this.yMin;
        }
        
        this.addDrawable(tile);
    }

    /**
     * @public
     */
    addEnemy(enemy) {
        this.addDrawable(enemy);
    }

    /**
     * @public
     */
    addProp(prop, layer) {
        this.addDrawable(prop, layer);
    }

    /**
     * @public
     * Returns true if the given rect is clear of solid obstacles.
     */
    rectIsClear(x, y, width, height, exclude, collisionGroups) {
        let rect;

        if (!isNaN(x)) {
            rect = {
                x,
                y,
                width,
                height,
            };
        } else {
            rect = x;
        }

        let obstacles;

        if (collisionGroups) {
            obstacles = [];

            for (let group of collisionGroups) {
                obstacles = obstacles.concat(group.collidesWith(rect));
            }
        } else {
            obstacles = this._tiles.concat(this._props);
        }

        for (let obstacle of obstacles) {
            if (obstacle === exclude) continue;
            
            if (obstacle.isSolid() && obstacle.collidesWithRect(rect)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @public
     */
    get tiles() {
        return this._tiles;
    }

    /**
     * @public
     */
    get enemies() {
        return this._enemies;
    }

    /**
     * @public
     */
    removeDrawable(d) {
        if (!d) return this;

        for (let i in this._layers) {
            if (!this._layers[i]) continue;
            
            this._layers[i] = Array.from(this._layers[i].filter(x => x !== d));
        }

        this._tiles = this._tiles.filter(d2 => d2 !== d);
        this._props = this._props.filter(d2 => d2 !== d);
        this._enemies = this._enemies.filter(d2 => d2 !== d);
        this._projectiles = this._projectiles.filter(d2 => d2 !== d);

        d.tearDown();

        d.collisionGroups.forEach((group) => {
            group.delete(d);
        });

        return this;
    }

    /**
     * @public
     * @param {Function} drawableClass - the constructor of the drawable to look for.
     * @return {?Drawable}
     */
    getDrawable(drawableClass) {
        for (let d of this.drawables) {
            if (d instanceof drawableClass) return d;
        }

        return null;
    }

    /**
     * @public
     * @param {Function} drawableClass - the constructor of the drawable to look for.
     * @return {Drawable[]}
     */
    getAllDrawables(drawableClass) {
        let result = [];

        for (let d of this.drawables) {
            if (d instanceof drawableClass) {
                result.push(d);
            }
        }

        return result;
    }

    /**
     * @public
     */
    getDrawablesInArea(x, y, width, height) {
        const rect = {x, y, width, height};

        return Array.from(this.drawables)
            .filter(x => x.collidesWithRect(rect));
    }

    /**
     * @public
     */
    getDrawableAtPoint(x, y) {
        const keys = Array.from(Object.keys(this._layers)).sort().reverse();

        for (let i of keys) {
            for (let j = this._layers[i].length - 1; j >= 0; j--) {
                const drawable = this._layers[i][j];

                if (drawable instanceof Player) continue;
                if (drawable instanceof Projectile) continue;

                if (drawable.pointIsInside(x, y)) {
                    return drawable;
                }
            }
        }

        return null;
    }

    /**
     * @public
     */
    removeDrawableAtPoint(x, y) {
        const drawable = this.getDrawableAtPoint(x, y);

        if (drawable) this.removeDrawable(drawable);
    }

    /**
     * @public
     */
    removeTile(tile) {
        this._tiles = this._tiles.filter(t => t !== tile);

        for (let i in this._layers) {
            this._layers[i] = this._layers[i].filter(d => d !== tile)
        }
    }

    /**
     * @public
     */
    removeProp(prop) {
        this._props = this._props.filter(p => p !== prop);

        for (let i in this._layers) {
            const propIndex = this._layers[i].indexOf(prop);

            if (propIndex > -1) {
                this._layers[i][propIndex].tearDown();

                this._layers[i].splice(propIndex, 1);
            }
        }
    }

    /**
     * @public
     */
    tearDown() {
        for (let i in this._layers) {
            for (let j in this._layers[i]) {
                this._layers[i][j].tearDown();
            }
            // Kick out players to prevent duplicates.
            this._layers[i] = this._layers[i].filter(d => !(d instanceof Player));
        }
    }

    /**
     * @public
     */
    update() {
        for (let layer of Object.values(this._layers)) {
            for (let d of layer) {
                d.update();
            }
        }

        this._updateProjectiles();
    }

    /**
     * @public
     */
    get props() {
        return this._props;
    }

    /**
     * @public
     * Returns an iterable over all drawables in the room;
     */
    get drawables() {
        const layerIs = Array.from(Object.keys(this._layers));
        let layerI = 0;
        let j = 0;
        let iterationCount = 0;

        let result = {};

        result[Symbol.iterator] = () => {return result;}

        result.next = () => {
            let done = false;
            let value = null;
            const layer = this._layers[layerIs[layerI]];

            if (layer && j < layer.length) {
                value = layer[j];
            }

            if (!layer) {
                done = true;
                value = iterationCount;
            } else {
                j++;
                iterationCount++;

                if (j >= layer.length) {
                    j = 0;
                    layerI++;
                }
            }

            return {
                value,
                done,
            };
        };

        return result;
    }

    /**
     * @private
     */
    _updateProjectiles() {
        // Handle projectile collisions.
        for (let layer of Object.values(this._layers)) {
            for (let drawable of layer) {
                if (!drawable.isSolid() && !drawable.projectileCollisions) continue;
                if (drawable instanceof Projectile) continue;

                this._projectiles.forEach(projectile => {
                    if (drawable.collidesWithRect(projectile.hitbox)) {
                        drawable.onProjectileCollision(projectile);
                    }
                });
            }
        }

        // Remove dead drawables.
        for (let i = this._minLayer; i <= this._maxLayer; i++) {
            if (!this._layers[i]) continue;

            for (let j in this._layers[i]) {
                const d = this._layers[i][j];

                if (!d) continue;

                if (d.isAlive && !d.isAlive()) {
                    this.removeDrawable(d);
                }
            }
        }
    }

    /**
     * @private
     */
    _pointIsInside(x, y) {
        return x >= 0 && x < this.width
            && y >= 0 && y < this.height;
    }

    /**
     * @public
     */
    drawBackground(xOff, k) {
        if (!this._background) {
            this.game.graphics.background(0);
        } else {
            this._background.draw(xOff, k);
        }
    }

    /**
     * @public
     */
    drawForeground(xOff, k) {
        k = k || 0.1;
        let i = 0;

        this._fgLayers.forEach(img => {
            let x = 0;

            while (x < this.width) {
                image(img, x - xOff * (i + 1) * k, 0);

                x += img.width;
            }

            i++;
        });
    }

    /**
     * @public
     */
    addDrawable(drawable, layer) {
        drawable.game = this.game;

        layer = layer || drawable.defaultLayer || 0;

        this._minLayer = Math.min(layer, this._minLayer);
        this._maxLayer = Math.max(layer, this._maxLayer);

        if (!this._layers[layer]) this._layers[layer] = [];

        drawable.load().then(() => {
            this._layers[layer].push(drawable);
            drawable.layer = layer;

            if (drawable instanceof Projectile) {
                this._projectiles.push(drawable);
            }

            if (drawable instanceof Prop) {
                this._props.push(drawable);
            }

            if (drawable instanceof Enemy) {
                this._enemies.push(drawable);
            }

            if (this._loaded)
                drawable.setup();

            drawable.collisionGroups.forEach((group) => {
                group.add(drawable);
            });
        });

        for (let i in drawable.children) {
            this.addDrawable(drawable.children[i], drawable.childLayers[i] || layer);
        }
    }

    /**
     * Sort a drawable to the top of its layer.
     * @public
     * @param {Drawable} drawable
     */
    moveToTopOfLayer(drawable) {

        for (let layer of Object.values(this._layers)) {
            const index = layer.indexOf(drawable);

            if (index > -1) {
                layer.splice(index, 1);
                layer.push(drawable);

                return;
            }
        }
    }

    /**
     * @public
     */
    draw() {
        for (let i = this._minLayer; i <= this._maxLayer; i++) {
            if (!this._layers[i]) continue;

            this._layers[i].forEach(drawable => {
                drawable.draw();
            });
        };

        if (this._darknessLevel) {
            this._drawLighting();
        }
    }

    /**
     * @private
     */
    _initDarkness() {
        if (!this._darknessLevel) return;
        
        const v = Math.round(255 * (1 - this._darknessLevel));
        let dark = new p5.Image(this.width, this.height);
        dark.loadPixels();

        for (let x = 0; x < dark.width; x++) {
            for (let y = 0; y < dark.height; y++) {
                dark.set(x, y, color(0, 0, 0, v));
            }
        }

        dark.updatePixels();

        this._darknessOverlay = dark;
    }

    /**
     * @private
     */
    _drawLighting() {
        const w = this._darknessOverlay.width;
        const h = this._darknessOverlay.height;
        let dark = new p5.Image(w, h);

        dark.copy(this._darknessOverlay, 0, 0, w, h, 0, 0, w, h);

        this._lightSources.forEach(s => {
            const mask = s.getMask();

            dark.mask(mask);
        });

        image(dark, 0, 0);
    }

    /**
     * @private
     */
    _preloadNeighbors() {
        const start = new Date().getTime();

        this._props.forEach(p => {
            if (p.destinationRoomIndex) {
                Game.world.getRoom(p.destinationRoomIndex).load();
            }
        });

        const time = new Date().getTime() - start;

        console.log('Preloaded rooms in ' + (time / 1000).toFixed(2) + 's');
    }

    /**
     * @public
     */
    load(preload) {
        if (preload) {
            this._preloadNeighbors();
        }

        if (this._loadPromise) return this._loadPromise;

        let promises = [];
        const start = new Date().getTime();
        const load = x => x.forEach(y => promises.push(y.load()));

        for (let i in this._layers) {
            load(this._layers[i]);
        }

        if (this._musicLoadPromise) promises.push(this._musicLoadPromise);
        if (this._background) promises.push(this._background.load());
        if (this._fgLoadPromise) promises.push(this._fgLoadPromise);

        this._loadPromise = Promise.all(promises).then(() => {
            const time = new Date().getTime() - start;

            console.log('Loaded room ' + this.name + ' in ' + (time / 1000).toFixed(2) + 's');
            this._loaded = true;
        });

        return this._loadPromise;
    }

    /**
     * @private
     */
    static _loadRoomFromURI(room) {
        const uri = room.split('?')[0];

        return Request.get(room + '?v=' + Math.random()).then(data => {
            room = this.fromJSON(data);
            room.uri = uri;
            
            return room.load().then(() => room);
        });
    }

    /**
     * @public
     */
    static load(room) {
        if (room instanceof Room) {
            return room.load(true).then(() => room);
        }

        return this._loadRoomFromURI(room);
    }

    /**
     * @public
     */
    static fromDict(d) {
        let room = new Room();
        
        if (d.name !== undefined)
            room.name = d.name;

        d.enemies.forEach(enemy => {
            let e = eval(enemy.class).fromDict(enemy);

            room.addEnemy(e);
        });

        d.props.forEach(prop => {
            room.addProp(eval(prop.class).fromDict(prop));
        });

        if (d.location) {
            room.x = d.location.x;
            room.y = d.location.y;
        }
        room._musicURI = d.music;
        if (d.background) room.setBackgroundLayers(d.background);
        if (d.foreground) room.setForegroundLayers(d.foreground);

        if (d.tiles) {
            d.tiles.forEach(t => {
                if (t instanceof String || typeof t === 'string')
                    t = JSON.parse(t);
                
                const tile = eval(t.class).fromDict(t);

                room.addTile(tile);
            });
        }

        room._darknessLevel = d._darknessLevel || 0;

        room._initDarkness();

        return room;
    }

    /**
     * @public
     */
    static fromJSON(json) {
        json = JSON.parse(json);

        return this.fromDict(json);
    }

    /**
     * @public
     */
    toJSON() {
        let result = {
            name: this.name,
            location: {
                x: this.x,
                y: this.y,
            },
        };

        const sortF = (a, b) => {
            if (a.x < b.x) return 1;
            if (a.x > b.x) return -1;
            if (a.y < b.y) return 1;
            if (a.y > b.y) return -1;

            return 0;
        }

        result.enemies = this._enemies.map(enemy => {
            return enemy.toDict();
        }).sort(sortF);

        result.props = this._props.map(prop => {
            return prop.toDict();
        }).sort(sortF);

        result.tiles = this._tiles.map(tile => {
            return tile.toDict();
        }).sort(sortF);

        result.music = this._musicURI;

        if (this._background && this._background.uris)
            result.background = this._background.uris;
        result.foreground = this._fgLayerURIs;
        result._darknessLevel = this._darknessLevel;

        return JSON.stringify(result);
    }
}