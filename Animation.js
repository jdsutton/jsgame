/**
 * @class
 */
class Animation {

    /**
     * @constructor
     */
    constructor(frameURIs, framerate, loop) {
        this._index = 0;
        this._stoppedAtIndex = 0;
        this._stopped = false;
        this._timeOffset = new Date().getTime();
        this._framerate = framerate || 24;
        this._frameDurationMs = 1000 / this._framerate;
        this._loop = loop;
        if (this._loop === undefined) this._loop = true;
        this._uris = frameURIs || [];
        this._images = [];
        this._direction = 1;
        this._loadPromise = null;
        this._onFinishedCallbacks = [];
    }

    /**
     * @public
     */
    setImages(images) {
        this._images = images;

        return this;
    }

    /**
     * @public
     */
    reset() {
        this._index = 0;
        this._stoppedAtIndex = 0;
        this._timeOffset = new Date().getTime();

        return this;
    }

    /**
     * @public
     */
    setReverse(value) {
        if (value) this._direction = -1;
        else this._direction = 1;

        return this;
    }

    /**
     * @public
     */
    setLoop(value) {
        this._loop = value;

        return this;
    }

    /**
     * @public
     */
    copy() {
        let result = new Animation(this._uris, this._framerate, this._loop);

        result._images = this._images;
        result._stoppedAtIndex = this._stoppedAtIndex;
        result._stopped = this._stopped;
        result._direction = this._direction;

        return result;
    }

    /**
     * @public
     */
    pause() {
        this._stoppedAtIndex = this._index;
        this._stopped = true;

        return this;
    }

    /**
     * @public
     */
    play() {
        if (!this._stopped) return this;

        this._timeOffset = new Date().getTime();
        this._stopped = false;

        return this;
    }

    /**
     * @public
     */
    static loadAll(dict) {
        const keys = Array.from(Object.keys(dict));
        let promises = [];

        keys.forEach(key => {
            const p = dict[key].load();

            promises.push(p);
        });

        return Promise.all(promises);
    }

    /**
     * @public
     * @return {Promise}
     */
    load() {
        if (this._loadPromise) return this._loadPromise;

        let promises = [];
        let i = 0;

        this._uris.forEach(uri => {
            const index = i;
            const p = JSImage.load(uri).then((img) => {
                this._images[index] = img;
            });

            promises.push(p);
            i++;
        });
    
        this._loadPromise = Promise.all(promises);

        return this._loadPromise;
    }

    /**
     * @public
     */
    getFrame(offset) {
        if (this._stopped) return this._images[this._stoppedAtIndex];

        const t = new Date().getTime() - this._timeOffset;
        const offsetFrames = Math.round(t / this._frameDurationMs) + (offset || 0);
        let index = offsetFrames + this._stoppedAtIndex;

        if (this._loop) {
            index %= this._images.length;
        } else {
            index = Math.min(index, this._images.length - 1);
            index = Math.max(index, 0);
            
            if (index === this._images.length - 1) {
                this._onFinishedCallbacks.forEach((c) => c());
            }
        }

        if (this._direction < 0) index = this._images.length - 1 - index;

        this._index = index;

        return this._images[index];
    }

    /**
     * @public
     * @param {Function} callback
     * @return {Animation} this
     */
    onFinished(callback) {
        this._onFinishedCallbacks.push(callback);

        return this;
    }

    // /**
    //  * @public
    //  */
    // render(x, y, offset, scale, frame) {
    //     scale = scale || this._scale;
    //     frame = frame || this.getFrame(offset);

    //     image(frame, x, y, frame.width * scale, frame.height * scale);
    // }
}