import('Drawable.js');
import('particles/ImgParticle.js');

/**
 * @class
 */
class Tile extends Drawable {
    
    /**
     * @constructor
     */
    constructor(tilesetURI, tilesetX, tilesetY, w, h, x, y, img) {
        super(x, y, w, h);

        this._tilesetURI = tilesetURI;
        this._tilesetX = tilesetX;
        this._tilesetY = tilesetY;
        this._imageLoadPromise = null;
        this._image = img || null;
        if (!this._image) {
            this.imgFromTileset(tilesetURI, tilesetX, tilesetY, w, h);
        } else {
            this._imageLoadPromise = Promise.resolve();
        }
        this._args = [tilesetURI, tilesetX, tilesetY, w, h, x, y];
    }

    /**
     * @public
     */
    dissolve(theta) {
        ImgParticle.dissolve(this.x, this.y, this._image, theta);
    }

    /**
     * @public
     * @return {Promise}
     */
    load() {
        return this._imageLoadPromise;
    }

    /**
     * @public
     */
    get args() {
        return this.args;
    }

    /**
     * @public
     */
    copy() {
        let result = new this.constructor(
            this._tilesetURI,
            this._tilesetX,
            this._tilesetY,
            this.width,
            this.height,
            this.x,
            this.y,
            this._image,
        );

        result.defaultLayer = this.defaultLayer;

        return result;
    }

    /**
     * @public
     */
    imgFromTileset(tilesetURI, tilesetX, tilesetY, w, h) {
        let key = `${tilesetURI}_${tilesetX}_${tilesetY}_${w}_${h}`;
        const promise = Tile._tileImagePromises[key];
        const memoized = Tile._tileImages[key];

        // Already loading.
        if (promise) {
            this._imageLoadPromise = promise;

            promise.then(img => {
                this._image = img;
            });

            return;
        }

        // Load image.
        let img = document.createElement('img');

        this._imageLoadPromise = new Promise((resolve, reject) => {
            img.onload = () => {
                this._image = Tile._loadTileFromImg(img, tilesetX, tilesetY, w, h, resolve);
                Tile._tileImages[key] = this._image;

                resolve(this._image);
            };
        });
        Tile._tileImagePromises[key] = this._imageLoadPromise;

        img.crossOrigin = 'anonymous';
        img.src = tilesetURI;
    }

    /**
     * @private
     */
    static _loadTileFromImg(img, tilesetX, tilesetY, w, h, resolve) {
        return JSImage.crop(img, tilesetX, tilesetY, w, h);
    }

    /**
     * @public
     */
    update() {}

    /**
     * @public
     * @abstract
     */
    onProjectileCollision(projectile) {}
}

Tile._calls = 0;
Tile._tileImages = {};
Tile._tileImagePromises = {};

Tile.TYPE = {
    NORMAL: 'normal',
};
