import('/src/JSUtils/io/Mouse.js');
import('/src/JSUtils/math/Random.js');
import('../AutoTilingMaterial.js');

/**
 * @class
 */
class Editor {

    /**
     * @public
     * @param {Game} game
     * @return {Editor} this
     */
    static setGame(game) {
        this._game = game;

        return this;
    }

    /**
     * @public
     */
    static get currentLayer() {
        return this._defaultLayer;
    }

    /**
     * @public
     */
    static onPlace(callback) {
        this._onPlaceCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    static onDelete(callback) {
        this._onDeleteCallbacks.push(callback);

        return this;
    }

    /**
     * @private
     */
    static _setPlaceable(p) {
        if (!Array.isArray(p)) p = [p];

        for (let i in p) {
            p[i] = p[i].copy();
        }

        const gameObj = p[0];

        if (this._placeableGameObj) {
            this._game.currentRoom.removeDrawable(this._placeableGameObj);
        }

        this._placeable = p;
        this._placeableGameObj = p[0];

        p.forEach(placeable => {
            placeable.defaultLayer = this._defaultLayer;
        });
        
        this._game.currentRoom.addDrawable(this._placeableGameObj);
    }

    /**
     * @public
     * @param {Function} f
     */
    static setCanPlace(f) {
        this._canPlaceF = f;

        return this;
    }

    /**
     * @public
     */
    static handleCanvasLeftClick() {
        if (!this._game || !this._game.inEditMode) return;
        
        if (this._placeable) {
            let p = Random.choice(this._placeable).copy();

            p.defaultLayer = this._defaultLayer;

            if (!this._canPlaceF || this._canPlaceF(p)) {
                this._placeF(p);
                p.onPlace();

                this._onPlaceCallbacks.forEach((c) => {
                    c(p);
                });
            }
        }
    }

    /**
     * @public
     */
    static handleCanvasRightClick() {
        if (!this._game) return;

        if (this._placeableGameObj) {
            this._game.currentRoom.removeDrawable(this._placeableGameObj);
            this._placeableGameObj = null;
            this._placeable = null;

        } else if (this._game.inEditMode) {
            const pos = this._game.getMousePosInGame();
            const room = this._game.currentRoom;

            const drawable = room.getDrawableAtPoint(pos.x, pos.y);

            if (drawable) {
                room.removeDrawable(drawable);

                this._onDeleteCallbacks.forEach((c) => {
                    c(drawable);
                });

                if (drawable.materialClassName) {
                    const material = AutoTilingMaterial.get(drawable.materialClassName);

                    material.handleDeleteAtPosition(drawable.x, drawable.y);
                }
            }
        }
    }

    /**
     * @private
     */
    static _setPlaceablePosition(obj) {
        let pos = this._game.getMousePosInGame();

        pos.x -= this._game.GRID_SIZE_PX / 2
        pos.y -= this._game.GRID_SIZE_PX / 2;

        let x = Math.round((pos.x - obj.width / 2) / this._game.GRID_SIZE_PX) * this._game.GRID_SIZE_PX;
        let y = Math.round((pos.y - obj.height / 2) / this._game.GRID_SIZE_PX) * this._game.GRID_SIZE_PX;
        
        obj.x = x;
        obj.y = y;
    };

    /**
     * @public
     */
    static update() {        
        if (this._placeableGameObj) {
            this._placeable.forEach(p => {
                this._setPlaceablePosition(p);
            });

            if (this._placeableGameObj instanceof AutoTilingMaterial && Mouse.buttonIsPressed(0)) {
                this._tryPlaceMaterial();
            }
        }
    }

    /**
     * @private
     */
    static _tryPlaceMaterial() {
        const t = new Date().getTime();

        if (t - this._lastMaterialPlaceTime < this._MATERIAL_PLACE_DELAY_MS) return;

        this._placeableGameObj.placeTileAtPosition();
        this._lastMaterialPlaceTime = t; 
    }

    /**
     * @public
     */
    static setPlaceableMaterial(material) {
        this._setPlaceable(material.copy());
        this._placeF = material => {};

        return this;
    }

    /**
     * @public
     */
    static setPlaceableTile(tile) {
        this._setPlaceable(tile);
        this._placeF = tile => this._game.currentRoom.addTile(tile);

        return this;
    }

    /**
     * @public
     */
    static setPlaceableEnemy(enemy) {
        this._setPlaceable(enemy);
        this._placeF = enemy => this._game.currentRoom.addEnemy(enemy);

        return this;
    }

    /**
     * @public
     */
    static setPlaceableProp(prop) {
        this._setPlaceable(prop);
        this._placeF = prop => this._game.currentRoom.addProp(prop);

        return this;
    }

    /**
     * @public
     */
    static getPlaceable() {
        return this._placeableGameObj;
    }

    /**
     * @public
     */
    static init() {
        // ConfigurableController.onInputReleased(Player.INPUT.MOD_3, () => {
        //     if (!this._game.inEditMode) return;
            
        //     this._defaultLayer++;

        //     if (this._placeableGameObj) {
        //         this._placeableGameObj.defaultLayer++;

        //         this._game.currentRoom
        //             .removeDrawable(this._placeableGameObj)
        //             .addDrawable(this._placeableGameObj);
        //     }
        // });

        // ConfigurableController.onInputReleased(Player.INPUT.MOD_4, () => {
        //     if (!this._game.inEditMode) return;

        //     this._defaultLayer--;

        //     if (this._placeableGameObj) {
        //         this._placeableGameObj.defaultLayer--;

        //         this._game.currentRoom
        //             .removeDrawable(this._placeableGameObj)
        //             .addDrawable(this._placeableGameObj);
        //     }
        // });
    }

    /**
     * @public
     */
    static hasPlaceable() {
        return Boolean(this._placeable);
    }
}

Editor._defaultLayer = 0;
Editor._placeable = null;
Editor._placeableGameObj = null;
Editor._canPlaceF = null;
Editor._placeF = null;
Editor._lastMaterialPlaceTime = 0;
Editor._MATERIAL_PLACE_DELAY_MS = 250;
Editor._game = null;
Editor._onPlaceCallbacks = [];
Editor._onDeleteCallbacks = [];
