import('/src/jsgame/animation/Animate.js');

/**
 * Used for controlling the viewpoint in the game.
 * @class
 */
class Camera {

    /**
     * @constructor
     * @param {Game} game
     */
    constructor(game) {
        this.game = game;

        /**
         * Whether to force the viewbox to stay inside the current room.
         * @type {Boolean}
         */
        this.constrainView = false;

        this._following = null;
        this._snapW = null;
        this._snapH = null;
        this.locked = false;
        this._viewX = 0;
        this._viewY = 0;
    }

    /**
     * @public
     */
    set x(val) {
        this._viewX = val;
    }

    /**
     * @public
     */
    set y(val) {
        this._viewY = val;
    }

    /**
     * @public
     */
    get x() {
        return this._viewX;
    }

    /**
     * @public
     */
    get y() {
        return this._viewY;
    }

    /**
     * Move the camera.
     * @param {Number} x
     * @param {Number} y
     */
    translate(x, y) {
        if (this._following) return this;

        this._viewX += x;
        this._viewY += y;

        return this;
    }

    /**
     * Do a smooth camera translation.
     * @public
     * @param {Number} dx - X translation
     * @param {Number} dy - Y translation
     * @param {?Number} durationMs - default 250ms
     */
    translateAnimated(dx, dy, durationMs, ignoreFollowing) {
        if (this.locked || (this._following && !ignoreFollowing)) return;

        this.locked = true;

        durationMs = durationMs || 250;

        const xPromise = Animate.variable(
            this,
            'x',
            this.x,
            this.x + dx,
            durationMs,
            Animate.INTERPOLATION.SIGMOID,
        );

        const yPromise = Animate.variable(
            this,
            'y',
            this.y,
            this.y + dy,
            durationMs,
            Animate.INTERPOLATION.SIGMOID,
        );

        const promises = [xPromise, yPromise];

        Promise.all(promises).then(() => {
            this.locked = false;
        });
    }

    /**
     * Move the camera.
     * @param {Number} x
     * @param {Number} y
     */
    setTranslation(x, y) {
        if (this._following) return this;

        this._viewX = x;
        this._viewY = y;

        return this;
    }

    /**
     * Track a particular object.
     * @public
     * @param {Drawable} drawable
     * @return {Camera} this
     */
    follow(drawable) {
        this._following = drawable;

        return this;
    }

    /**
     * Track a particular object, snapping to a grid
     * @public
     * @param {Drawable} drawable
     * @return {Camera} this
     * @return {Number} snapW
     * @return {Number} snapH
     */
    followSnap(drawable, snapW, snapH) {
        this._following = drawable;
        this._snapW = snapW;
        this._snapH = snapH;

        return this;
    }

    /**
     * Reset to initial state.
     * @public
     * @return {Camera} this
     */
    reset() {
        this._following = null;
        this._snapW = null;
        this._snapH = null;;

        return this;
    }

    /**
     * Update the camera, call before Game.draw().
     * @public
     */
    update() {
        const w = this.game.viewWidth;
        const h = this.game.viewHeight;
        const room = this.game.currentRoom;

        if (this._following) {
            if (this._snapW) {
                if (!this.locked) {
                    this.x = Math.round(this.x / this._snapW) * this._snapW;
                    this.y = Math.round(this.y / this._snapH) * this._snapH;
                }
                
                if (this._following.x < this.x) {
                    this.translateAnimated(-this._snapW, 0, 250, true);
                } else if (this._following.x > this.x + this._snapW) {
                    this.translateAnimated(this._snapW, 0, 250, true);
                } else if (this._following.y < this.y) {
                    this.translateAnimated(0, -this._snapH, 250, true);
                } else if (this._following.y > this.y + this._snapH) {
                    this.translateAnimated(0, this._snapH, 250, true);
                }
            } else {
                this._viewX = this._following.x - w / 2;
                this._viewY = this._following.y + this._following.height / 2 - h / 2;
            }
        }

        if (this.constrainView) {
            this._viewX = Math.max(room.xMin, this._viewX);
            this._viewX = Math.min(room.xMax - this.viewWidth, this._viewX);
            this._viewY = Math.max(room.yMin, this._viewY);
            this._viewY = Math.min(room.yMax - this.viewHeight, this._viewY);
        }

        this.game.setViewOffset(this._viewX, this._viewY);
    }
}