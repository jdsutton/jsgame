import('ConfigurableController.js');
import('Player.js');
import('Projectile.js');
import('/src/JSUtils/io/Keys.js');

/**
 * @class
 */
class SidescrollShooterPlayer extends Player {

    /**
     * @constructor
     */
    constructor(x, y) {
        super(x, y);

        this._xDir = 1;
        this._state = Player._STATE.STAND;
        this._jumpState = Player._JUMP_STATE.LAND;
        this._projectileClass = Projectile;
        this._aimState = Player._AIM_STATE.FORWARD;
        this._invulnerable = false;
        this._lastFireTimeMs = 0;
        this.health = this.constructor._DEFAULT_HEALTH;
        this._jumpDisabled = false;
    }

    /**
     * @public
     */
    setJumpEnabled(value) {
        this._jumpDisabled = !value;

        return this;
    }

    /**
     * @public
     */
    init() {
        this.health = this.constructor._DEFAULT_HEALTH;
    }

    /**
     * @public
     */
    recharge() {
        this.health = this.constructor._DEFAULT_HEALTH;
    }

    /**
     * @public
     */
    loadStateFromDict(dict) {
        Object.assign(this, dict);
    }

    /**
     * @public
     */
    initControls() {
        console.assert(this.game);
        
        ConfigurableController
            .setInput(
                Player.INPUT.JUMP,
                new KeyDownBooleanInput(KEYS.SPACE).or(
                    new GamepadButtonBooleanInput(0, 0)
                ),
            ).setInput(
                Player.INPUT.UP,
                new KeyDownBooleanInput(KEYS.W_KEY).or(
                    new GamepadAxisBooleanInput(0, 1, -1)
                ),
            ).setInput(
                Player.INPUT.DOWN,
                new KeyDownBooleanInput(KEYS.S_KEY).or(
                    new GamepadAxisBooleanInput(0, 1, 1)
                ),
            ).setInput(
                Player.INPUT.LEFT,
                new KeyDownBooleanInput(KEYS.A_KEY).or(
                    new GamepadAxisBooleanInput(0, 0, -1)
                ),
            ).setInput(
                Player.INPUT.RIGHT,
                new KeyDownBooleanInput(KEYS.D_KEY).or(
                    new GamepadAxisBooleanInput(0, 0, 1)
                ),
            ).setInput(
                Player.INPUT.FIRE,
                new MouseButtonBooleanInput(0, this.game).or(
                    new GamepadButtonBooleanInput(0, 1)
                ),
            ).setInput(
                Player.INPUT.AIM_UP,
                new KeyDownBooleanInput(KEYS.Z_KEY).or(
                    new GamepadButtonBooleanInput(0, 7)
                ),
            ).setInput(
                Player.INPUT.AIM_DOWN,
                new KeyDownBooleanInput(KEYS.X_KEY).or(
                    new GamepadButtonBooleanInput(0, 6)
                ),
            ).setInput(
                Player.INPUT.MOD_1,
                new KeyDownBooleanInput(KEYS.Q_KEY).or(
                    new GamepadButtonBooleanInput(0, 3)
                )
            ).setInput(
                Player.INPUT.MOD_2,
                new KeyDownBooleanInput(KEYS.F_KEY).or(
                    new GamepadButtonBooleanInput(0, 2)
                )
            ).setInput(
                Player.INPUT.MOD_3,
                new KeyDownBooleanInput(KEYS.Y_KEY).or(
                    new GamepadButtonBooleanInput(0, 12)
                )
            ).setInput(
                Player.INPUT.MOD_4,
                new KeyDownBooleanInput(KEYS.U_KEY).or(
                    new GamepadButtonBooleanInput(0, 13)
                )
            ).setInput(
                Player.INPUT.RB,
                new KeyDownBooleanInput(KEYS.R_KEY).or(
                    new GamepadButtonBooleanInput(0, 5)
                )
            ).setInput(
                Player.INPUT.SELECT,
                new KeyDownBooleanInput(KEYS.SHIFT).or(
                    new GamepadButtonBooleanInput(0, 8)
                )
            )
        ;
            
        ConfigurableController
            .onInputPressed(Player.INPUT.UP, () => {
                if (!this.game.canControlPlayer()) return;

                this._onUp();
            })
            .onInputReleased(Player.INPUT.UP, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputPressed(Player.INPUT.AIM_UP, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputReleased(Player.INPUT.AIM_UP, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputPressed(Player.INPUT.AIM_DOWN, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputReleased(Player.INPUT.AIM_DOWN, () => {
                if (!this.game.canControlPlayer()) return;

                this._updateAimState();
            })
            .onInputReleased(Player.INPUT.LEFT, () => {
                this.idle();
            })
            .onInputReleased(Player.INPUT.RIGHT, () => {
                this.idle();
            });

        ConfigurableController.onInputPressed(Player.INPUT.JUMP, () => {
            if (!this.game.canControlPlayer()) return;                
            
            if (this._canJump()) {
                this._jump();
            }
        });

        ConfigurableController.onInputReleased(Player.INPUT.JUMP, () => {
            // TODO: Refactor into this.game.
            if (this.game.cutsceneIsActive()) {
                this.game.advanceCutscene();
            
                return;                
            }
            if (!this.game.canControlPlayer()) return

            if (this._jumpState === Player._JUMP_STATE.JUMP && this._vy < 0) {
                this._vy = 0;
            }
        });

        ConfigurableController.onInputPressed(Player.INPUT.DOWN, () => {
            if (!this.game.canControlPlayer()) return;

            this._crouch();
        })
        .onInputReleased(Player.INPUT.DOWN, () => {
            if (!this.game.canControlPlayer()) return;
            
            this._updateAimState();
        });
    }

    /**
     * @public
     */
    onProjectileCollision(p) {
        if (!p.hurtPlayer) return;
        
        p.kill();

        this.hurt(p, p.damage);
    }

    /**
     * @public
     */
    isSolid() {
        return true;
    }

    /**
     * @private
     */
    _tryAimAtMouse() {
        if (!this.game.mouseIsInsideGameCanvas()) return;

        const pos = this.game.getMousePosInGame();
        const theta = Math.atan2(pos.y - (this.y + this.height / 2), pos.x - this.x);
        const delta = Math.PI / 8;
        const up = -Math.PI / 2;
        const upOver1 = -Math.PI / 4;
        const upOver2 = -Math.PI + Math.PI / 4;
        const downOver1 = Math.PI / 4;
        const downOver2 = Math.PI - Math.PI / 4;
        const down = Math.PI / 2;
        const prevXDir = this._xDir;

        if (this._vx === 0) {
            if (pos.x < this.x) {
                this._xDir = -1;
            } else {
                this._xDir = 1;
            }
        }

        if (this._xDir !== prevXDir) this._onAimStateChanged();

        if (theta > upOver1 - delta && theta < upOver1 + delta) {
            this._aim(Player._AIM_STATE.UP_OVER);
        } else if (theta > upOver2 - delta && theta < upOver2 + delta) {
            this._aim(Player._AIM_STATE.UP_OVER);
        } else if (theta > downOver1 - delta && theta < downOver1 + delta) {
            this._aim(Player._AIM_STATE.DOWN_OVER);
        } else if (theta > downOver2 - delta && theta < downOver2 + delta) {
            this._aim(Player._AIM_STATE.DOWN_OVER);
        } else if (theta > up - delta && theta < up + delta) {
            this._aim(Player._AIM_STATE.UP);
        } else if (theta > down - delta && theta < down + delta) {
            // TODO: Only allow aim down while jumping.
            this._aim(Player._AIM_STATE.DOWN_OVER);
            // this._aim(Player._AIM_STATE.DOWN);
        } else {
            this._aim(Player._AIM_STATE.FORWARD);
        }
    }

    /**
     * @private
     */
    _aim(newState) {
        const prevAimState = this._aimState;

        this._aimState = newState;

        if (this._aimState !== prevAimState) {
            this._onAimStateChanged();
            // this.idle();
        }
    }

    /**
     * @public
     */
    _updateAimState() {
        if (this.game.mouseIsInsideGameCanvas()) return;

        const up = ConfigurableController.getInput(Player.INPUT.UP);
        const down = ConfigurableController.getInput(Player.INPUT.DOWN);

        if (up) {
            this._aim(Player._AIM_STATE.UP);
        } else if (ConfigurableController.getInput(Player.INPUT.AIM_UP)) {
            this._aim(Player._AIM_STATE.UP_OVER);
        } else if (ConfigurableController.getInput(Player.INPUT.AIM_DOWN)) {
            this._aim(Player._AIM_STATE.DOWN_OVER);
        } else {
            this._aim(Player._AIM_STATE.FORWARD);
        }
    }

    /**
     * @public
     * @overide
     */
    get hitbox() {
        return {
            x: this._x,
            y: this._y,
            width: 12,
            height: this.height,
        };
    }

    /**
     * @public
     */
    asDict() {
        return {
            x: this.x,
            y: this.y,
        };
    }

    /**
     * @private
     * @abstract
     */
    _onIdleLeft() {}

    /**
     * @private
     * @abstract
     */
    _onIdleRight() {}

    /**
     * @private
     * @abstract
     */
    _onCrouch() {}

    /**
     * @private
     * @abstract
     */
    _onCrawl() {}

    /**
     * @private
     * @abstract
     */
    _onAimStateChanged() {}

    /**
     * @private
     */
    getLauncherOffset() {
        return {
            x: 0,
            y: 0,
        };
    }

    /**
     * @private
     */
    _getMoveSpeed() {
        return this.constructor._RUN_SPEED;
    }

    /**
     * @public
     */
    getAimTheta() {
        let theta = 0;

        if (this._aimState === Player._AIM_STATE.UP_OVER) {
            theta = -Math.PI / 4;
        } else if (this._aimState === Player._AIM_STATE.DOWN_OVER) {
            theta = Math.PI / 4;
        } else if (this._aimState === Player._AIM_STATE.UP) {
            theta = -Math.PI / 2;
        } else if (this._aimState === Player._AIM_STATE.DOWN) {
            theta = Math.PI / 2;
        }

        if (this._xDir < 0) theta = Math.PI - theta;

        return theta;
    }

    /**
     * @private
     */
    _makeProjectiles() {
        const theta = this.getAimTheta();

        const offset = this.getLauncherOffset();

        let shot = new (this._projectileClass)(
            this.x + offset.x,
            this.y + offset.y,
            this.constructor._SHOT_VELOCITY,
            theta,
        ).offsetVelocity(this._vx, 0);

        return [shot];
    }

    /**
     * @private
     */
    _canFire() {
        return new Date().getTime() - this._lastFireTimeMs
                > this.constructor._FIRE_DELAY_MS;
    }

    /**
     * @private
     */
    _fire() {
        if (!this._canFire()) return;
        if (this.game.paused) return;

        this._lastFireTimeMs = new Date().getTime();

        for (let p of this._makeProjectiles()) {
            p.playFireFx();
            this.game.currentRoom.addProjectile(p);
        }
    }

    /**
     * @private
     */
    _crouch() {
        if (this.game.paused) return;

        if (this._state === Player._STATE.STAND) {
            this._state = Player._STATE.CROUCH;
            this._onCrouch();
        } else if (this._state === Player._STATE.CROUCH) {
            this._state = Player._STATE.CRAWL;
            this._onCrawl();
        } else {
            this._updateAimState();
        }
    }

    /**
     * @public
     */
    idle() {
        if (this.game.paused) return;

        this._vx = 0;

        if (this._xDir < 0) {
            this._onIdleLeft();
        } else {
            this._onIdleRight();
        }
    }

    /**
     * @private
     */
    _canStandUp() {
        return true;
    }

    /** 
     * @private
     */
    _onUp() {
        if (!this.game.canControlPlayer()) return;
        if (!this._canStandUp()) return;

        if (this._state === Player._STATE.CRAWL) {
            this._state = Player._STATE.CROUCH;

            this.idle();
        } else if (this._state === Player._STATE.CROUCH) {
            this._state = Player._STATE.STAND;

            this.idle();
        } else {
            this._updateAimState();
        }
    }

    /**
     * @public
     * @override
     */
    update() {
        if (!this.game.canControlPlayer()) return;

        this._tryAimAtMouse();

        if (ConfigurableController.getInput(Player.INPUT.FIRE)) {
            this._fire();
        }           
        if (ConfigurableController.getInput(Player.INPUT.RIGHT)) {
            if (this._state === Player._STATE.CROUCH) this._state = Player._STATE.STAND;
            this._vx = this._getMoveSpeed();
        }
        if (ConfigurableController.getInput(Player.INPUT.LEFT)) {
            if (this._state === Player._STATE.CROUCH) this._state = Player._STATE.STAND;
            this._vx = -this._getMoveSpeed();
        }

        if (!this._invulnerable && Math.abs(this._vx) > 0) {
            this._xDir = Math.sign(this._vx);
        }

        super.update();

        if (!this._positionIsClear(0, this.constructor._GRAVITY)
            && this._jumpState === Player._STATE.JUMP) {
            this._land();
        }

        if (this._vy !== 0) this._jumpState = Player._JUMP_STATE.JUMP;
    
        this._handlePropCollisions();
        this._handleEnemyCollisions();
    }

    /**
     * @private
     */
    _canJump() {
        return !this._jumpDisabled
            && this._jumpState !== Player._JUMP_STATE.JUMP
            && this._positionIsClear(0, -Player._JUMP_VELOCITY);
    }

    /**
     * @private
     */
    _getJumpVelocity() {
        return this.constructor._JUMP_VELOCITY;
    }

    /**
     * @private
     * @abstract
     */
    _onJump() {};

    /**
     * @private
     */
    _jump() {
        if (this.game.paused) return;

        if (this._state === Player._STATE.CROUCH) {
            this._state = Player._STATE.STAND;
        } else {
            this._jumpState = Player._JUMP_STATE.JUMP;
            this._vy = -this._getJumpVelocity();

            this._onJump();
        }
    }

    /**
     * @private
     * @override
     */
    _land() {
        this._vy = 0;
        this._jumpState = Player._JUMP_STATE.LAND;
    }

    /**
     * @public
     */
    draw() {
        const hitbox = this.hitbox;
        const graphics = this.game.graphics;

        graphics.fill(255, 0, 0);
        graphics.stroke(255);
        
        graphics.rect(
            Math.round(hitbox.x),
            Math.round(hitbox.y),
            hitbox.width,
            hitbox.height,
        );
    }

    /**
     * @public
     */
    drawHUD() {
        const hitbox = this.hitbox;
        const total = this.health / this.constructor._MAX_HEALTH;
        const barWidth = 64;
        const barHeight = 8;
        const pad = 8;
        const graphics = this.game.graphics;

        graphics.fill(0, 0, 255);
        graphics.noStroke();

        graphics.rect(pad, pad, 100 * total, barHeight);

        graphics.strokeWeight(1);
        graphics.noFill();
        graphics.stroke(0);
        graphics.line(pad, pad, pad + barWidth, pad);
        graphics.line(pad, pad, pad, pad + barHeight);
        graphics.stroke(255);
        graphics.line(barWidth + pad, pad, barWidth + pad, pad + barHeight);
        graphics.line(pad, pad + barHeight, barWidth + pad, pad + barHeight);
    }
}
