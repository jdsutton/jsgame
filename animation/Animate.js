import('/src/JSUtils/math/CubicInterpolation.js');

/**
 * @class
 */
class Animate {

    /**
     * @public
     * @param {Object} obj - Reference to an object
     * @param {String} key - A key in obj
     * @param {Number} startValue
     * @param {Number} endValue
     * @param {Number} durationMs
     * @param {Animate.INTERPOLATION} interpolation
     * @return {Promise}
     */
    static variable(obj, key, startValue, endValue, durationMs, interpolation) {
        const startTime = new Date().getTime();

        interpolation = interpolation || Animate.INTERPOLATION.LINEAR;

        return new Promise((resolve, reject) => {
            window.requestAnimationFrame(() => {
                this._step((x) => {obj[key] = x;}, startTime, resolve, startValue, endValue, durationMs, interpolation);
            });
        });
    }

    /**
     * @public
     * @param {Function} callback
     * @param {Number} startValue
     * @param {Number} endValue
     * @param {Number} durationMs
     * @param {Animate.INTERPOLATION} interpolation
     * @return {Promise}
     */
    static callback(callback, startValue, endValue, durationMs, interpolation) {
        const startTime = new Date().getTime();

        interpolation = interpolation || Animate.INTERPOLATION.LINEAR;

        return new Promise((resolve, reject) => {
            window.requestAnimationFrame(() => {
                this._step(callback, startTime, resolve, startValue, endValue, durationMs, interpolation);
            });
        });
    }

    /**
     * @private
     */
    static _step(callback, startTime, resolve, startValue, endValue, durationMs, interpolation) {
        const t = new Date().getTime();
        const endTime = startTime + durationMs;
        let x = (t - startTime) / durationMs;

        if (interpolation === Animate.INTERPOLATION.CUBIC) {
            x = new CubicInterpolation(startValue, startTime, endValue, endTime).compute(t);
        } else if (interpolation === Animate.INTERPOLATION.SIGMOID) {
            x = 3 * Math.pow(x, 2) - 2 * Math.pow(x, 3);
        }

        callback((endValue - startValue) * x + startValue);

        if (t < endTime) {
            window.requestAnimationFrame(() => {
                this._step(...arguments);
            });
        } else {
            callback(endValue);
            resolve();
        }
    }
}

Animate.INTERPOLATION = {
    LINEAR: 0,
    CUBIC: 1,
    SIGMOID: 2,
};