import('JSImage.js');

/**
 * @class
 */
class Tileset {
    /**
     * @public
     */
    constructor(imagePath, tileWidth, tileHeight) {
        this._imagePath = imagePath;
        this._tileWidth = tileWidth || 32;
        this._tileHeight = tileHeight || this._tileWidth;
        this._img = null;
        this._tilesByXY = {};
    }

    /**
     * @public
     */
    static load(uri, tileWidth, tileHeight) {
        return JSImage.load(uri).then((img) => {
            let result = new Tileset(uri, tileWidth, tileHeight);
            result._img = img;

            return result;
        });
    }

    /**
     * @public
     */
    getTile(x, y) {
        if (!this._tilesByXY[x]) {
            this._tilesByXY[x] = {};
        }

        if (!this._tilesByXY[x][y]) {
            const xx = x * this._tileWidth;
            const yy = y * this._tileHeight;

            this._tilesByXY[x][y] = JSImage.crop(this._img, xx, yy, this._tileWidth, this._tileHeight);
        }

        return this._tilesByXY[x][y];
    }

    /**
     * @public
     */
    load() {
        return JSImage.load(this._imagePath).then((img) => {
            this._img = img;

            return this;
        });
    }
}