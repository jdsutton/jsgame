import('../Prop.js');
import('../particles/FallingWaterParticle.js');
import('/src/JSUtils/math/Random.js');

/**
 * @class
 */
class WaterfallProp extends Prop {

    /**
     * @public
     * @override
     */
    update() {
        for (let i = 0; i < this.constructor._DENSITY; i++) {
            let x = Random.range(0, this.width);
            const p = new FallingWaterParticle(this.x + x, this.y);

            Game.currentRoom.addParticle(p);
        }
    }
}

WaterfallProp._DENSITY = 3;
