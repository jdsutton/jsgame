import('ImageProp.js');

/**
 * @class
 */
class DraggableProp extends ImageProp {

    /**
     * @constructor
     */
    constructor() {
        super(...arguments);

        this._dragging = false;
        this._onRelease = [];
        this._handle = null;

        this._draggable = (() => true);
    }

    /**
     * @public
     */
    setup() {
        super.setup();

        this._handle = new Handle(
            () => this.hitbox, this.game.canvas)
            .onDragUp(dy => {
                if (!this._draggable()) return;

                this._dragging = true;

                this.y += dy;
            })
            .onDragDown(dy => {
                if (!this._draggable()) return;

                this._dragging = true;

                this.y += dy;
            })
            .onDragRight(dx => {
                if (!this._draggable()) return;

                this._dragging = true;

                this.x += dx;
            })
            .onDragLeft(dx => {
                if (!this._draggable()) return;

                this._dragging = true;

                this.x += dx;
            })
            .onRelease(() => {
                this._dragging = false;

                this._onRelease.forEach(f => f());
            });
    }
    /**
     * @public
     */
    onRelease(callback) {
        this._onRelease.push(callback);

        return this;
    }

    /**
     * @public
     */
    get dragging() {
        return this._dragging;
    }

    /**
     * @public
     */
    setDraggable(draggable) {
        this._draggable = draggable;

        return this;
    }

    /**
     * @public
     */
    draw() {
        const graphics = this.game._graphics;

        if (this._draggable()) {
            graphics.noStroke();
            graphics.fill('rgba(248, 248, 58, 0.25)');

            const box = this.hitbox;

            graphics.ellipse(
                this.x + box.width / 2,
                this.y + box.height / 2,
                64,
                64
            );
        }

        super.draw();
    }
}
