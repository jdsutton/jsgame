import('../Prop.js');

/**
 * @class
 */
class ProximityTriggerable extends Prop {

    /**
     * @constructor
     */
    constructor() {
        super(...arguments);

        this._canTrigger = false;
    }

    /**
     * @private
     * @abstract
     */
    _trigger() {}

    /**
     * @private
     */
    _onDrawWithinProximity(player) {
        stroke(255);
        fill(255, 0, 0);

        textSize(18);
        text('!', player.x, player.y - 2);
    }

    /**
     * @public
     * @override
     */
    draw() {
        super.draw();

        const player = Game.getPlayer();
        const x = this.x + this.width / 2;
        const y = this.y + this.height / 2;
        const dist = new Vector(player.x - x, player.y - y).magnitude();

        if (dist < this.constructor._INTERACT_DISTANCE) {
            this._onDrawWithinProximity(player);

            player.setJumpEnabled(false);

            if (!Game.cutsceneIsActive()
                && ConfigurableController.getInput(Player.INPUT.JUMP)) {
                this._trigger();
            }
        } else {
            player.setJumpEnabled(true);
        }
    }
}

ProximityTriggerable._INTERACT_DISTANCE = 32;
