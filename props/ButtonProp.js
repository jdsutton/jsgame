import('../Prop.js');
import('../Handle.js');

/**
 * @class
 */
class ButtonProp extends Prop {

    /**
     * @constructor
     */
    constructor(x, y, w, h) {
        super(...arguments);

        if (h === undefined) this.height = 32;
        if (w === undefined) this.width = Math.round(this.height * 1.618);
        this._backgroundColor = 0;
        this._activeBackgroundColor = 0;
        this._active = false;
        this._label = 'Button';
        this._onClick = () => {
            console.log('ButtonProp clicked');
        };
   }

   /**
    * @public
    */
    setup() {
        this._handle = new Handle(
            () => {
                return this.hitbox;
            },
            this.game.canvas,
        ).onClick(() => {
            this._onClick();
        });
    }

   /**
    * @public
    */
    setLabel(label) {
        this._label = label;

        return this;
    }

   /**
    * @public
    */
    setOnClick(f) {
        this._onClick = f;

        return this;
    }

    /**
     * @public
     */
    setBackgroundColor(c) {
        this._backgroundColor = c;

        return this;
    }

    /**
     * @public
     */
    setActiveBackgroundColor(c) {
        this._activeBackgroundColor = c;

        return this;
    }

    /**
     * @public
     */
    setActive(value) {
        this._active = value;

        return this;
    }

    /**
     * @public
     */
    draw() {
        const graphics = this.game.graphics;

        if (this._active) {
            graphics.fill(this._activeBackgroundColor);
        } else {
            graphics.fill(this._backgroundColor);
        }

        graphics.noStroke();

        graphics.rect(this.x, this.y, this.width, this.height);

        graphics.fill('#FFF');

        graphics.textAlign(Shapes.ALIGN.CENTER, Shapes.ALIGN.CENTER);
        graphics.text(this._label, this.x + this.width / 2, this.y + this.height / 2);
    }
}