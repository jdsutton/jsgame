/**
 * @class
 */
class JSProp extends Prop {

    /**
     * @constructor
     */
    constructor(x, y, f) {
        super(x, y);

        this._f = f;
    }

    /**
     * @public
     */
    draw() {
        this._f();
    }
}
