/**
 * @class
 */
class ImageProp extends Prop {

    /**
     * @constructor
     */
    constructor(x, y, imageURI, width, height) {
        super(x, y, width, height);

        this._imageURI = imageURI;

        this.vx = 0;
        this.vy = 0;
    }

    /**
     * @public
     */
    update() {
        super.update();

        this._x += this.vx;
        this._y += this.vy;
    }
}
