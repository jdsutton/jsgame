import('../Prop.js');

/**
 * @class
 */
class TileProp extends Prop {

    /**
     * @constructor
     */
    constructor() {
        super(...arguments);

        /* {String} Refers to a subclass of AutoTilingMaterial */
        this.materialClassName = null;
    }

    /**
     * @public
     */
    setup() {
        super.setup();
        
        // this.collisionGroups.push(this.game.COLLISION.TILES);
        this.game.COLLISION.TILES.add(this);

        this.width = this.game.GRID_SIZE_PX;
        this.height = this.game.GRID_SIZE_PX;
    }

    /**
     * @public
     * @override
     */
    copy() {
        let result = super.copy();

        result.materialClassName = this.materialClassName

        return result;
    }


    /**
     * @public
     */
    tearDown() {
        const index = this.game.COLLISION.TILES.delete(this);
    }

    /**
     * @public
     */
    isSolid() {
        return true;
    }
}