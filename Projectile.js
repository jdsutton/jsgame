import('particles/Particle.js');

/**
 * @class
 */
class Projectile extends Particle {

    /**
     * @constructor
     */
    constructor() {
        super(...arguments);

        this.damage = 1;
        this.hurtEnemies = true;
        this.hurtPlayer = false;
    }

    copy() {
        let result = new (this.constructor)(this.x, this.y);

        result._vx = this._vx;
        result._vy = this._vy;

        result.width = this.width;
        result.height = this.height;
        result.damage = this.damage;

        return result;
    }

    /**
     * @public
     * @abstract
     */
    playFireFx() {}
}
