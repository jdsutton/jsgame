# JSGame
Open HTML5 game programming framework. Supports 2D games with a built-in level editor.

## Installation
Depends on [JSUtils](https://gitlab.com/jdsutton/JSUtils), built with [Slurp](https://gitlab.com/jdsutton/Slurp)

Use the following directory structure:

* /myproject
    * /src
        * /JSUtils
        * /jsgame

## Documentation
[https://jdsutton.gitlab.io/jsgame/](https://jdsutton.gitlab.io/jsgame/)
