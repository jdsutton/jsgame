import('Room.js');
import('/src/JSUtils/datastructures/KDTree.js');

/**
 * @class
 */
class DynamicCullingRoom extends Room {

    /** 
     * @constructor
     */
    constructor() {
        super(...arguments);

        this._indexedLayers = [];
        this._indexedTiles = new KDTree([], tile => [tile.x, tile.y], 2);
        this._radius = this.constructor._DEFAULT_RADIUS;
    }

    /**
     * @private
     * @override
     */
    _deleteDrawable(d) {
        super._deleteDrawable(d);

        // TODO TODO TODO.
    }

    /**
     * @public
     * @override
     */
    addTile(tile) {
        super.addTile(tile);

        this._indexedTiles.addItem(tile);
    }

    /**
     * @public
     * @override
     */
    update() {
        const player = Planetoid.getPlayer();
        const point = [player.x, player.y];
        const tiles = this._indexedTiles.nearestN(point);

        tiles.forEach(tile => tile.update());

        // this._updateProjectiles();
    }

    /**
     * @public
     * @override
     */
    draw() {
        // for (let i = this._minLayer; i <= this._maxLayer; i++) {
        //     if (!this._layers[i]) continue;

        //     this._layers[i].forEach(drawable => {
        //         drawable.draw();
        //     });
        // };

        if (this._darknessLevel) {
            this._drawLighting();
        }
    }

    // rectIsClear() {}
}

DynamicCullingRoom._DEFAULT_RADIUS = 128;
