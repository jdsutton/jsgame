import('MenuItem.js');

/**
 * @class
 */
class MenuText extends MenuItem {

    /**
     * @constructor
     */
    constructor(getLabelF, x, y) {
        super();
        
        this.getLabelF = getLabelF;
        this.x = x;
        this.y = y;
        this.align = Shapes.ALIGN.CENTER;
        this.textSize = 12;
        this._textFill = 255;
    }

    /**
     * @public
     * @param {String|Function} label
     * @return {MenuText} this
     */
    setLabel(label) {
        this.getLabelF = label;

        return this;
    }

    /**
     * @public
     * @param {Number|String} fill
     * @return {MenuText} this
     */
    setTextFill(fill) {
        this._textFill = fill;

        return this;
    }

    /**
     * @public
     * @param {Shapes.ALIGN} align
     * @return {MenuText} this
     */
    setAlign(align) {
        this.align = align;

        return this;
    }

    /**
     * @public
     * @param {Number} align
     * @return {MenuText} this
     */
    setTextSize(textSize) {
        this.textSize = textSize;

        return this;
    }
  
    /**
     * @public
     */
    draw() {
        const graphics = this.game.graphics;

        graphics.textAlign(this.align, Shapes.ALIGN.CENTER);

        graphics.fill(this._textFill);
        graphics.noStroke();
        graphics.textSize(this.textSize)

        let t = this.getLabelF;

        if (!(t instanceof String || typeof t === 'string')) {
            t = t();
        }

        graphics.text(t, this.x, this.y);
    }
}