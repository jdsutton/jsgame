import('../Drawable.js');

/**
 * @class
 */
class MenuItem extends Drawable {

    /**
     * @constructor
     */
    constructor() {
        super();
        
        this._xOff = 0;
        this._yOff = 0;
        this.width = 128;
        this.height = 12;
        this.textSize = 12;
    }

    /**
     * @public
     */
    setOffset(x, y) {
        if (x !== null)
            this._xOff = x;

        if (y !== null)
            this._yOff = y;

        return this;
    }
}