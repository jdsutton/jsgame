import('MenuButton.js');
import('MenuText.js');

/**
 * @class
 */
class Menu extends Drawable {

    /**
     * @constructor
     */
    constructor(config) {
        super();

        config = config || {};
        this._drawables = [[]];
        this.x = config.x || 0;
        this.y = config.y || 0;
        this._buttons = [[]];
        this._selectedButtonIndex = 0;
        this._columnIndex = 0;
        this._numColumns = 1;
        this._textSize = config.textSize || 12;
        this._columnWidth = config.columnWidth || 32;
        this._activeMenu = this;
        this._parentMenu = null;
        this._scroll = false;
        this._exitOnFire = true;
    }

    /**
     * @public
     */
    set game(val) {
        this._game = val;

        for (let col in this._drawables) {
            for (let d of this._drawables[col]) {
                if (d === this) continue;

                d.game = val;
            }
        }
    }

    /**
     * @public
     */
    get game() {
        return this._game;
    }

    /**
     * @public
     */
    get x() {
        return Math.round(this._x);
    }

    /**
     * @public
     */
    get y() {
        return Math.round(this._y);
    }

    /**
     * @public
     * @override
     */
    set x(val) {
        this._x = val;

        for (let i in this._drawables) {
            for (let d of this._drawables[i]) {
                d.setOffset(this.x + this._getColumnX(i), this.y);
            }
        }
    }

    /**
     * @public
     * @override
     */
    set y(val) {
        this._y = val;

        for (let i in this._drawables) {
            for (let d of this._drawables[i]) {
                d.setOffset(this.x + this._getColumnX(i), this.y);
            }
        }
    }

    /**
     * @private
     */
    _handleClick() {
        const pos = this.game.getMousePosOnScreen();

        this._buttons.forEach((col) => {
            col.forEach((button) => {
                if (button.pointIsInside(pos.x, pos.y)) {
                    button.click();
                }
            });
        });
    }

    setup() {
        for (let col of this._buttons) {
            for (let button of col) {
                button.game = this.game;
            }
        }
    }

    /**
     * @public
     */
    scroll(dir) {
        this._activeMenu._selectedButtonIndex += dir;

        this._activeMenu._constrainIndices();
    }

    /**
     * @public
     */
    setExitOnFire(value) {
        this._exitOnFire = Boolean(value);

        return this;
    }

    /**
     * @public
     */
    clear() {
        this._buttons = [[]];
        this._drawables = [[]];

        this.setNumColumns(this._numColumns);

        return this;
    }

    /**
     * @public
     */
    setNumColumns(n) {
        this._numColumns = n;

        while(this._drawables.length < n) 
            this._drawables.push([]);
        while(this._buttons.length < n) 
            this._buttons.push([]);

        return this;
    }

    /**
     * @public
     */
    setColumnWidth(w) {
        this._columnWidth = w;

        return this;
    }

    /**
     * @public
     */
    allowScroll(value) {
        this._scroll = value;

        return this;
    }

    /**
     * @public
     */
    get buttons() {
        let result = [];

        for (let buttons of this._buttons)
            result = result.concat(buttons);

        return result;
    }

    /**
     * @public
     * @override
     */
    update() {
        this.buttons.forEach((b) => {
            b.update();
        });

        super.update();
    }

    /**
     * @public
     * @override
     */
    load() {
        const buttonPromises = Array.from(this.buttons.map((b) => b.load()));
        let promises = [super.load()].concat(buttonPromises);

        return Promise.all(promises);
    }

    /**
     * @private
     */
    _getColumnX(i) {
        let result = -(this._numColumns - 1) * this._columnWidth / 2;

        return result + (i || 0) * this._columnWidth;
    }

    /**
     * @public
     */
    addButton(button, columnIndex) {
        button.game = this.game;
        button.load();

        button.setOffset(
            this.x + this._getColumnX(columnIndex), this.y);

        this._buttons[columnIndex || 0].push(button);
        this._drawables[columnIndex || 0].push(button);

        return this;
    }

    /**
     * @private
     */
    _getSelectedButton() {
        return this._buttons[this._columnIndex][this._selectedButtonIndex];
    }

    /**
     * @public
     */
    addText(text, columnIndex) {
        text.game = this.game;

        this._drawables[columnIndex || 0].push(text);

        return this;
    }

    /**
     * @private
     */
    _gotoParent() {
        let node = this;

        // Goto top node.
        while (node._parentMenu) node = node._parentMenu;

        if (node._activeMenu._parentMenu) {
            node._activeMenu = node._activeMenu._parentMenu;
        }
    }

    /**
     * @private
     */
    enterSubmenu(menu) {
        menu._parentMenu = this._activeMenu;

        this._activeMenu = menu;
    }

    /**
     * @private
     */
    _getColSize(index) {
        return this._drawables[index || 0].length 
    }

    /**
     * @private
     */
    _constrainIndices() {
        this._columnIndex = (this._columnIndex + this._numColumns)
            % this._numColumns;

        if (!this._buttons[this._columnIndex].length) {
            this._columnIndex = 0;

            while (this._columnIndex < this._buttons.length
                && !this._buttons[this._columnIndex].length) {
                this._columnIndex++;
            }
        }

        const buttons = this._buttons[this._columnIndex];

        if (!buttons) return;

        if (buttons.length === 0) {
            this._selectedButtonIndex = 0;
        } else {
            this._selectedButtonIndex = (this._selectedButtonIndex + buttons.length) % buttons.length;
        }
    }

    /**
     * @private
     */
    _handleInput() {
        const up = ConfigurableController.getInput(KEYS.ARROW_UP)
            || ConfigurableController.getInput(Player.INPUT.UP);
        const down = ConfigurableController.getInput(KEYS.ARROW_DOWN)
            || ConfigurableController.getInput(Player.INPUT.DOWN);
        const left = ConfigurableController.getInput(KEYS.ARROW_LEFT)
            || ConfigurableController.getInput(Player.INPUT.LEFT);
        const right = ConfigurableController.getInput(KEYS.ARROW_RIGHT)
            || ConfigurableController.getInput(Player.INPUT.RIGHT);
        const enter = ConfigurableController.getInput(KEYS.ENTER_KEY);
        const back = ConfigurableController.getInput(KEYS.ESCAPE_KEY);

        this._constrainIndices();

        if (new Date().getTime() - Menu._lastInputMs > Menu._INPUT_DELAY_MS) {
            if (up) {
                this._selectedButtonIndex--;
                this._constrainIndices();
                Menu._lastInputMs = new Date().getTime();
            } else if (down) {
                this._selectedButtonIndex++;
                this._constrainIndices();
                Menu._lastInputMs = new Date().getTime();
            } else if (left) {
                this._columnIndex--;
                this._constrainIndices();
                Menu._lastInputMs = new Date().getTime();
            } else if (right) {
                this._columnIndex++;
                this._constrainIndices();
                Menu._lastInputMs = new Date().getTime();
            } else if (enter) {
                this._buttons[this._columnIndex][this._selectedButtonIndex].click();
                Menu._lastInputMs = new Date().getTime();
            } else if (back && this._exitOnFire) {
                this._gotoParent();
            }
        }

    }

    /**
     * @private
     */
    _draw() {
        super.draw();
        
        const graphics = this.game.graphics;

        graphics.push();
        graphics.textSize(this._textSize);
        
        graphics.translate(this.x, this.y);

        this._handleInput();

        if (this._scroll) {
            const butt = this._buttons[this._columnIndex][this._selectedButtonIndex];
            const offset = this.game.viewHeight / 2 - butt.y - butt.height / 2;

            this.buttons.forEach(b => b.setOffset(null, offset));
            
            graphics.translate(0, offset);
        }

        for (let i = 0; i < this._drawables.length; i++) {
            let buttonI = 0;

            graphics.push();
            graphics.translate(this._getColumnX(i), 0);
            
            for (let j = 0; j < this._drawables[i].length; j++) {
                const d = this._drawables[i][j];

                if (d instanceof MenuButton) {
                    const selected = i === this._columnIndex
                        && buttonI === this._selectedButtonIndex;

                    d.draw(selected);

                    buttonI++;
                } else d.draw();
            }

            graphics.pop();
        }

        graphics.pop();
    }

    /**
     * @public
     */
    draw() {
        this._activeMenu._draw();
    }

    /**
     * @public
     * @abstract
     */
    tearDown() {}
}

Menu._lastInputMs = 0;
Menu._INPUT_DELAY_MS = 175;