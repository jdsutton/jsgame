import('MenuItem.js');

/**
 * @class
 */
class MenuButton extends MenuItem {

    /**
     * @constructor
     */
    constructor(label, x, y, width, height) {
        super();

        this.label = label;
        this.x = x;
        this.y = y;
        this.width = width || 256;
        this.height = height || 32;
        this._onClick = null;
        this._bgColor = 0;
    }

    /**
     * @public
     */
    setOnClick(callback) {
        this._onClick = callback;

        return this;
    }

    /**
     * @public
     */
    click() {
        if (this._onClick) this._onClick();
    }

    /**
     * @public
     */
    pointIsInside(x, y) {
        x -= this._xOff;
        y -= this._yOff;

        return (x >= this.x - this.width / 2
            && x < this.x + this.width / 2
            && y >= this.y - this.height / 2
            && y < this.y + this.height / 2);
    }

    /**
     * @public
     */
    draw(active) {
        const graphics = this.game.graphics;

        graphics.textAlign(Shapes.ALIGN.CENTER, Shapes.ALIGN.CENTER);

        graphics.fill(this._bgColor);
        graphics.stroke(255);
        graphics.strokeWeight(1);
        if (active) {
            graphics.strokeWeight(2);
            graphics.stroke(129, 212, 250);
        }
        graphics.rect(this.x - this.width / 2, this.y - this.height / 2, this.width, this.height);

        graphics.fill(255);
        graphics.noStroke();

        graphics.textSize(this.textSize);

        if (this.label instanceof Function) {
            graphics.text(this.label(), this.x, this.y, this.width);
        }
        else {
            graphics.text(this.label, this.x, this.y, this.width);
        }
    }
}