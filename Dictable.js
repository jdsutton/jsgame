/**
 * @class
 */
class Dictable {

    /**
     * @public
     */
    toDict() {
        let result = {
            class: this.constructor.name,
            args: this._args,
            layer: this.defaultLayer
        };

        const transform = x => {
            if (x instanceof Dictable) return x.toDict();

            return x;
        };

        this.constructor._PERSIST_PROPERTIES.forEach(key => {
            if (this[key] === undefined) return;

            if (Array.isArray(this[key])) {
                result[key] = this[key].map(transform);
            } else {
                result[key] = transform(this[key]);
            }
        });

        return result;
    }

    /**
     * @public
     */
    static fromDict(dict) {
        const args = dict.args || [];
        let result = new (eval(dict.class))(...args);

        if (dict.layer !== undefined)
            result.defaultLayer = dict.layer;

        const transform = x => {
            if (x.class) return eval(x.class).fromDict(x);

            return x;
        };

        this._PERSIST_PROPERTIES.forEach(key => {
            const property = dict[key];

            if (property === undefined) return;

            if (Array.isArray(property)) {
                result[key] = property.map(transform);
            } else {
                result[key] = transform(property);
            }
        });

        return result;
    }
}

Dictable._PERSIST_PROPERTIES = [];